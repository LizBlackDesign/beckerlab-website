<?php

class DatabaseConfiguration {
    const HOST = 'localhost';
    const DATABASE_NAME = 'fsr';
    const USERNAME = 'root';
    const PASSWORD = 'root';
}

class User {
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $host;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $hashedPass;
    /**
     * @var string
     */
    public $salt;
    /**
     * @var string
     */
    public $role;
    /**
     * @var bool
     */
    public $isActive;
}

class AuthCookie {
    /**
     * The Hashing Function (Available to PHP)
     * Used To Hash The Authenticator
     */
    const HASH_ALGORITHM = 'sha512';

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $hashedAuthenticator;

    /**
     * @var DateTime
     */
    public $expires;

    /**
     * Set's Up AN Auth Cookie
     * Hashes The Password According To HASH_ALGORITHM
     *
     * @param int $userId
     * @param string $plaintextAuthenticator
     * @param int $expires
     * Unix Timestamp For When This Cookie Should Expire
     * @return AuthCookie
     */
    public static function create(int $userId, string $plaintextAuthenticator, int $expires): AuthCookie {
        $cookie = new AuthCookie();

        $cookie->userId = $userId;
        $cookie->hashedAuthenticator = AuthCookie::hashAuthenticator($plaintextAuthenticator);
        $cookie->expires = DateTime::createFromFormat('U', $expires);

        return $cookie;
    }

    /**
     * Hashes The Plaintext String According To The HASH_ALGORITHM
     *
     * @param string $plaintext
     * The String To Hash
     *
     * @return string
     * The Hashed Authenticator
     */
    public static function hashAuthenticator(string $plaintext): string {
        return hash(self::HASH_ALGORITHM, $plaintext);
    }

    /**
     * Checks If A Plaintext String Matches The Authenticator
     *
     * @param string $plaintext
     * The Plaintext String To Test
     *
     * @return bool
     * True If The Hashed Version Of $plaintext Matches $hashedAuthenticator
     * False Otherwise
     */
    public function checkAuthenticator(string $plaintext): bool {
        return hash_equals($this->hashedAuthenticator,  AuthCookie::hashAuthenticator($plaintext));
    }
}

class Request {
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $username;
    /**
     * @var DateTime
     */
    public $created;
    /**
     * @var string
     */
    public $class;
    /**
     * @var int
     */
    public $drives;
    /**
     * @var string
     */
    public $operatingSystem;
    /**
     * @var string
     */
    public $other;
    /**
     * @var string
     */
    public $status;
}

class Lab {
    /**
     * @var int
     */
    public $reservation_id;
    /**
     * @var int
     */
    public $faculty_id;
    /**
     * @var string
     */
    public $dept;
    /**
     * @var int
     */
    public $reserving_class;
    /**
     * @var string
     */
    public $reserving_section;
    /**
     * @var bool
     */
    public $is_teaching_lab;
    /**
     * @var DateTime
     */
    public $date;
    /**
     * @var DateTime
     */
    public $start_time;
    /**
     * @var DateTime
     */
    public $end_time;

    /**
     * @var int
     */
    public $repeating_reservation_id;

    /**
     * @var string
     */
    public $faculty_username;

}

class Repeating_Reservation{
    /**
     * @var int
     */
    public $repeating_res_id;
    /**
     * @var DateTime
     */
    public $start_date;
    /**
     * @var DateTime
     */
    public $end_date;
    /**
     * @var DateTime
     */
    public $start_time;
    /***
     * @var DateTime
     */
    public $end_time;
    /**
     * @var bool
     */
    public $monday;
    /**
     * @var bool
     */
    public $tuesday;
    /**
     * @var bool
     */
    public $wednesday;
    /**
     * @var bool
     */
    public $thursday;
    /**
     * @var bool
     */
    public $friday;
    /**
     * @var string
     * This field is the whole reserving class code in the following format
     * Dept ClassNum-SectionNum
     */
    public $reserving_class;
    /**
     * @var bool
     */
    public $is_teaching_lab;
    /**
     * @var int
     */
    public $faculty_id;
}

class Schedule{
    /**
     * @var int
     */
    public $scheduleNumber;

    /**
     * @var DateTime
     */
    public $startActiveDate;

    /**
     * @var DateTime
     */
    public $endActiveDate;
}

class EmployeeShift{
    /**
     * @var int
     */
    public $shiftNumber;
    /**
     * @var int
     */
    public $employeeId;
    /**
     * @var string
     */
    public $dayOfShift;
    /**
     * @var DateTime
     */
    public $shiftStart;
    /**
     * @var DateTime
     */
    public $shiftEnd;
    /**
     * @var int
     */
    public $scheduleNumber;
}

class RawEmployeeShift extends EmployeeShift {
    /**
     * @var string
     */
    public $type;

    public static function createFromShift(EmployeeShift $employeeShift = null, string $type): RawEmployeeShift {
        $raw = new RawEmployeeShift();
        if (!is_null($employeeShift)) {
            $raw->shiftNumber = $employeeShift->shiftNumber;
            $raw->employeeId = $employeeShift->employeeId;
            $raw->dayOfShift = $employeeShift->dayOfShift;
            $raw->shiftStart = $employeeShift->shiftStart;
            $raw->shiftEnd = $employeeShift->shiftEnd;
            $raw->scheduleNumber = $employeeShift->scheduleNumber;
            $raw->type = $type;
        }

        return $raw;
    }

}

class ImperfectShift {
    /**
     * @var int
     */
    public $imperfect_shift_number;
    /**
     * @var int
     */
    public $employeeId;
    /**
     * @var int
     */
    public $shiftNumber;
    /**
     * @var int
     */
    public $timeclockShiftId;
    /**
     * @var DateTime
     */
    public $shiftDate;
    /**
     * @var string
     */
    public $type;
    /**
     * @var int
     */
    public $mins;

    /**
     * @var bool
     */
    public $excused = false;
    /**
     * @var string
     */
    public $reason;
    /**
     * @var string
     */
    public $other;
}

class ImperfectShiftInformation extends ImperfectShift{
    /**
     * @var string
     */
    public $shiftTime;
    /**
     * @var
     */
    public $shiftEnd;
    /**
     * @var string
     */
    public $firstName;
    /**
     * @var string
     */
    public $lastName;
}

class Holiday{
    /**
     * @var int
     */
    public $holidayId;
    /**
     * @var string
     */
    public $holidayName;
    /**
     * @var DateTime
     */
    public $holidayStartDate;
    /**
     * @var DateTime
     */
    public $holidayEndDate;
}

class Event {
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     * Must be in the ISO8601 format
     */
    public $start;

    /**
     * Background Color of The Event On The Calendar
     * @var string
     */
    public $color;

    /**
     * @var string
     * Must be in the ISO8601 format
     */
    public $end;
}

class ReservationOutput {
    /**
     * @var string
     */
    public $reserving_faculty_username;

    /**
     * @var int
     */
    public $reserving_faculty_id;

    /**
     * @var string
     */
    public $reserving_class;

    /**
     * @var int
     * Boolean integer
     */
    public $is_teaching_lab;

    /**
     * @var DateTime
     */
    public $start_time;

    /**
     * @var DateTime
     */
    public $end_time;

    /**
     * @var DateTime
     */
    public $reservation_date;

}

class RepeatingReservationOutput extends ReservationOutput {
    /**
     * @var DateTime
     */
    public $start_date;
    /**
     * @var DateTime
     */
    public $end_date;
    /**
     * @var integer
     */
    public $repeating_res_id;
    /**
     * @var string
     */
    public $days_string;
}

function _getConnection() {
    $dsn = 'mysql:host=' . DatabaseConfiguration::HOST . ';dbname=' . DatabaseConfiguration::DATABASE_NAME;

    return new \PDO($dsn, DatabaseConfiguration::USERNAME, DatabaseConfiguration::PASSWORD);
}

function _generatePassword(string $plaintext_pass, string $salt) {
    return hash('sha512', $salt . $plaintext_pass);
}


function verifyPassword(string $plaintext, string $salt, string $hashed) {
    return hash('sha512', $salt . $plaintext) == $hashed;
}

function persistUser(User $user) {
    if (!isset($user->id))
        return _insertUser($user);
    else
        return _updateUser($user);
}

function _insertUser(User $user): int {
    $db = _getConnection();
    $query = "INSERT INTO `users` (username, host, email, password, salt, is_active, role) VALUES (:Username, :Host, :Email, :Pass, :Salt, :Is_Active, :Role)";
    $statement = $db->prepare($query);

    $statement->bindValue(':Username', $user->username);
    $statement->bindValue(':Host', $user->host);
    $statement->bindValue(':Email', $user->email);

    if ($user->host === 'localhost')
        $statement->bindValue(':Pass', $user->hashedPass);
    else
        $statement->bindValue(':Pass', null, PDO::PARAM_NULL);

    if ($user->host === 'localhost')
        $statement->bindValue(':Salt', $user->salt);
    else
        $statement->bindValue(':Salt', null, PDO::PARAM_NULL);

    $statement->bindValue(':Is_Active', ($user->isActive) ? 1 : 0);
    $statement->bindValue(':Role', $user->role);

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}

function _updateUser(User $user): int {
    $db = _getConnection();
    $query = "UPDATE `users` SET `username`=:Username, `host`=:Host, `email`=:Email,  `password`=:Pass, `salt`=:Salt, `is_active`=:Is_Active, `role`=:Role WHERE `id`=:Id";
    $statement = $db->prepare($query);

    $statement->bindValue(':Username', $user->username);
    $statement->bindValue(':Host', $user->host);
    $statement->bindValue(':Email', $user->email);
    $statement->bindValue(':Pass', $user->hashedPass);
    $statement->bindValue(':Salt', $user->salt);
    $statement->bindValue(':Is_Active', ($user->isActive) ? 1 : 0);
    $statement->bindValue(':Role', $user->role);
    $statement->bindValue(':Id', $user->id);

    $statement->execute();
    $statement->closeCursor();

    return $user->id;
}

function _userFromRow($result) {
    $user = new User();

    $user->id = $result['id'];
    $user->username = $result['username'];
    $user->host = $result['host'];
    $user->email = $result['email'];

    // Passwords Are Returned Conditionally
    if (isset($result['password']))
        $user->hashedPass = $result['password'];
    if (isset($result['salt']))
        $user->salt = $result['salt'];

    $user->isActive = $result['is_active'] > 0;
    $user->role = $result['role'];

    return $user;
}

function getUserById(int $id) {
    $db  = _getConnection();
    $query = "SELECT `id`, `username`, `host`, `email`, `password`, `salt`, `is_active`, `role` FROM `users` WHERE id=:Id LIMIT 1";

    $statement = $db->prepare($query);
    $statement->bindValue(':Id', $id);
    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    if ($result) {
        return _userFromRow($result);
    } else {
        return null;
    }
}

/**
 * Returns A User Without The Password Fields (password/salt) Set
 * @param int $id
 * @return User
 */
function getReducedUserById(int $id) {
    $db  = _getConnection();
    $query = "SELECT `id`, `username`, `host`, `email`, `is_active`, `role` FROM `users` WHERE id=:Id LIMIT 1";

    $statement = $db->prepare($query);
    $statement->bindValue(':Id', $id);
    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    if (count($result) > 0) {
        return _userFromRow($result);
    } else {
        return null;
    }
}

function getUserByUsername(string $username) {
    $db  = _getConnection();
    $query = "SELECT `id`, `username`, `host`, `email`, `password`, `salt`, `is_active`, `role` FROM `users` WHERE `username`=:Username LIMIT 1";

    $statement = $db->prepare($query);
    $statement->bindValue(':Username', $username);
    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    if ($result) {
        return _userFromRow($result);
    } else {
        return null;
    }
}

function deleteUser(int $id) {
    $db = _getConnection();
    $statement = $db->prepare("DELETE FROM `users` WHERE `id`=:Id");
    $statement->bindValue(":Id", $id);
    $statement->execute();
    $statement->closeCursor();
}

function getAllUsers(): array {
    $db  = _getConnection();
    $query = "SELECT `id`, `username`, `host`, `email`, `is_active`, `role` FROM `users`";

    $statement = $db->prepare($query);
    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _userFromRow($result);
    }
    return $parsedResults;
}

function _authCookieFromRow($row): AuthCookie {
    $auth = new AuthCookie();

    $auth->id = intval($row['id']);
    $auth->userId = intval($row['user_id']);
    $auth->hashedAuthenticator = $row['authenticator'];
    $auth->expires = DateTime::createFromFormat('Y-m-d', $row['expires']);

    return $auth;
}

function persistAuthCookie(AuthCookie $authCookie): int {
    if (isset($authCookie->id))
        return _updateAuthCookie($authCookie);
    else
        return _insertAuthCookie($authCookie);
}

function _updateAuthCookie(AuthCookie $authCookie): int {
    $db = _getConnection();
    $query = "UPDATE `auth_cookie` SET `user_id`=:UserId, `authenticator`=:Authenticator, `expires`=:Expires WHERE `id` = :Id";
    $statement = $db->prepare($query);

    $statement->bindValue(':UserId', $authCookie->userId);
    $statement->bindValue(':Authenticator', $authCookie->hashedAuthenticator);
    $statement->bindValue(':Expires', $authCookie->expires->format('Y-m-d'));
    $statement->bindValue(':Id', $authCookie->id);

    $statement->execute();
    $statement->closeCursor();
    return $authCookie->id;
}

function _insertAuthCookie(AuthCookie $authCookie): int {
    $db = _getConnection();
    $query = "INSERT INTO `auth_cookie` (user_id, authenticator, expires) VALUES (:UserId, :Authenticator, :Expires)";
    $statement = $db->prepare($query);

    $statement->bindValue(':UserId', $authCookie->userId);
    $statement->bindValue(':Authenticator', $authCookie->hashedAuthenticator);
    $statement->bindValue(':Expires', $authCookie->expires->format('Y-m-d'));

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}


function getAuthCookieById(int $id, string $hashedAuthenticator) {
    $db = _getConnection();
    $query = "SELECT `id`,`user_id`, `authenticator`, `expires` FROM `auth_cookie` 
WHERE `id`=:Id AND `authenticator`=:Authenticator  AND `expires` > NOW() LIMIT 1";
    $statement = $db->prepare($query);

    $statement->bindValue(':Id', $id, PDO::PARAM_INT);
    $statement->bindValue(':Authenticator', $hashedAuthenticator);
    $statement->execute();

    $result = $statement->fetch();
    $statement->closeCursor();

    if ($result)
        return _authCookieFromRow($result);
    else
        return null;

}

function deleteAuthCookieById(int $id) {
    $db = _getConnection();
    $query = "DELETE FROM `auth_cookie` WHERE `id`=:Id";
    $statement = $db->prepare($query);

    $statement->bindValue(':Id', $id, PDO::PARAM_INT);
    $statement->execute();
    $statement->closeCursor();
}

function deleteOldAuthCookies() {
    $db = _getConnection();
    $query = "DELETE FROM `auth_cookie` WHERE `expires`<= NOW()";
    $statement = $db->prepare($query);

    $statement->execute();
    $statement->closeCursor();
}

const REQUEST_QUERY =  "SELECT `request`.`id` AS `id`, `user_id`, `users`.`username` AS `username`,
  `created`, `class`, `drives`, `operating_system`, `other`, `status` 
FROM `request`
INNER JOIN users users ON request.user_id = users.id";

function _requestFromRow($row): Request {
    $request = new Request();

    $request->id = intval($row['id']);
    $request->userId = intval($row['user_id']);
    $request->username = $row['username'];
    $request->created = DateTime::createFromFormat('Y-m-d H:i:s', $row['created']);
    $request->class = $row['class'];
    $request->drives = intval($row['drives']);
    $request->operatingSystem = $row['operating_system'];
    $request->other = $row['other'];
    $request->status = $row['status'];

    return $request;
}

function getRequestById(int $id) {
    $db  = _getConnection();
    $query =  REQUEST_QUERY ." WHERE `request`.`id`=:Id";

    $statement = $db->prepare($query);
    $statement->bindValue(':Id', $id);
    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    if (count($result) > 0) {
        return _requestFromRow($result);
    } else {
        return null;
    }
}

function getRequestsForUser(int $userId): array {
    $db  = _getConnection();
    $query = REQUEST_QUERY . " WHERE `user_id`=:UserId ORDER BY `created` DESC ";

    $statement = $db->prepare($query);
    $statement->bindValue(':UserId', $userId);
    $statement->execute();

    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _requestFromRow($result);
    }

    return $parsedResults;
}

function getRequestsForUserByStatus(int $userId, string $status): array {
    $db  = _getConnection();
    $query = REQUEST_QUERY . " WHERE `user_id`=:UserId AND `status`=:Status ORDER BY `created` DESC";

    $statement = $db->prepare($query);
    $statement->bindValue(':UserId', $userId);
    $statement->bindValue(':Status', $status);
    $statement->execute();

    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _requestFromRow($result);
    }

    return $parsedResults;
}

function getRequestsByStatus(string $status): array {
    $db  = _getConnection();
    $query = REQUEST_QUERY . " WHERE `status`=:Status ORDER BY `created` DESC";

    $statement = $db->prepare($query);
    $statement->bindValue(':Status', $status);
    $statement->execute();

    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _requestFromRow($result);
    }

    return $parsedResults;
}

function getAllRequests(): array {
    $db  = _getConnection();
    $query = REQUEST_QUERY ." ORDER BY `created` DESC";

    $statement = $db->prepare($query);
    $statement->execute();

    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _requestFromRow($result);
    }

    return $parsedResults;
}

function _insertRequest(Request $request): int {
    $db = _getConnection();
    $query = "INSERT INTO `request` (user_id, class, drives, operating_system, other, status, created) VALUES (:UserId, :Class, :Drives, :OperatingSystem, :Other, :Status, NOW())";
    $statement = $db->prepare($query);

    $statement->bindValue(':UserId', $request->userId);
    $statement->bindValue(':Class', $request->class);
    $statement->bindValue(':Drives', $request->drives);
    $statement->bindValue(':OperatingSystem', $request->operatingSystem);
    $statement->bindValue(':Other', $request->other);
    $statement->bindValue(':Status', $request->status);

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}

function _updateRequest(Request $request): int {
    $db = _getConnection();
    $query = "UPDATE `request` SET `class`=:Class, `drives`=:Drives, `operating_system`=:OperatingSystem, `other`=:Other ,`status`=:Status WHERE `id`=:Id";

    $statement = $db->prepare($query);
    $statement->bindValue(':Class', $request->class);
    $statement->bindValue(':Drives', $request->drives);
    $statement->bindValue(':OperatingSystem', $request->operatingSystem);
    $statement->bindValue(':Other', $request->other);
    $statement->bindValue(':Status', $request->status);
    $statement->bindValue(':Id', $request->id);

    $statement->execute();
    $statement->closeCursor();

    return $request->id;
}

function persistRequest(Request $request): int {
    if (!isset($request->id)) {
        return _insertRequest($request);
    } else {
        return _updateRequest($request);
    }
}

function deleteRequest(int $id) {
    $db  = _getConnection();
    $query = "DELETE FROM `request` WHERE `id`=:Id";

    $statement = $db->prepare($query);
    $statement->bindValue(':Id', $id);
    $statement->execute();
    $statement->closeCursor();
}

function insertSchedule(Schedule $schedule): int {
    $db = _getConnection();
    $query = "INSERT INTO `schedule` (start_active_date, `end_active_date`) VALUES (:StartActiveDate,:EndActiveDate)";

    $statement = $db->prepare($query);
    $statement->bindValue(':StartActiveDate', $schedule->startActiveDate->format('Y-m-d'));
    $statement->bindValue(':EndActiveDate', $schedule->endActiveDate->format('Y-m-d'));

    $statement->execute();
    $statement->closeCursor();

    return $db->lastInsertId();
}

function insertHoliday(Holiday $holiday): int {
    $db = _getConnection();
    $query = "INSERT INTO `holiday` (`holiday_name`, `holiday_start_date`, `holiday_end_date`) VALUES (:holidayName, :startDate,:endDate)";

    $statement = $db->prepare($query);
    $statement->bindValue(':holidayName', $holiday->holidayName);
    $statement->bindValue(':startDate', $holiday->holidayStartDate->format('Y-m-d'));
    $statement->bindValue(':endDate', $holiday->holidayEndDate->format('Y-m-d'));

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}

function insertEmployeeShift(EmployeeShift $employeeShift): int {
    $db = _getConnection();
    $query = "INSERT INTO `employee_shift` (`employee_id`, `day_of_shift`, `shift_start`, `shift_end`, `schedule_number`) VALUES (:EmployeeId, :DayOfShift, :ShiftStart, :ShiftEnd, :ScheduleNumber)";

    $statement = $db->prepare($query);
    $statement->bindValue(':EmployeeId', $employeeShift->employeeId);
    $statement->bindValue(':DayOfShift', $employeeShift->dayOfShift);
    $statement->bindValue(':ShiftStart', $employeeShift->shiftStart->format('H:i:s'));
    $statement->bindValue(':ShiftEnd', $employeeShift->shiftEnd->format('H:i:s'));
    $statement->bindValue(':ScheduleNumber', $employeeShift->scheduleNumber);

    $statement->execute();
    $statement->closeCursor();

    return $db->lastInsertId();
}

function insertManyEmployeeShift(array $employeeShifts): array {
    $insertedIds = [];
    foreach ($employeeShifts as $employeeShift) {
         $insertedIds[] = insertEmployeeShift($employeeShift);
    }
    return $insertedIds;
}

function insertRawEmployeeShift(RawEmployeeShift $rawEmployeeShift): int {
    $db = _getConnection();
    $query = "INSERT INTO `raw_employee_shift` (`employee_id`, `day_of_shift`, `type`,`shift_start`, `shift_end`, `schedule_number`) VALUES (:EmployeeId, :DayOfShift, :Type, :ShiftStart, :ShiftEnd, :ScheduleNumber)";

    $statement = $db->prepare($query);
    $statement->bindValue(':EmployeeId', $rawEmployeeShift->employeeId);
    $statement->bindValue(':DayOfShift', $rawEmployeeShift->dayOfShift);
    $statement->bindValue(':Type', $rawEmployeeShift->type);
    $statement->bindValue(':ShiftStart', $rawEmployeeShift->shiftStart->format('H:i:s'));
    $statement->bindValue(':ShiftEnd', $rawEmployeeShift->shiftEnd->format('H:i:s'));
    $statement->bindValue(':ScheduleNumber', $rawEmployeeShift->scheduleNumber);

    $statement->execute();
    $statement->closeCursor();

    return $db->lastInsertId();
}

function insertManyRawEmployeeShifts(array $rawEmployeeShifts): array {
    $insertedIds = [];
    foreach ($rawEmployeeShifts as $rawEmployeeShift) {
        $insertedIds[] = insertRawEmployeeShift($rawEmployeeShift);
    }
    return $insertedIds;
}

function _rawEmployeeShiftFromRow($result): RawEmployeeShift {
    $shift = new \RawEmployeeShift();

    $shift->shiftNumber = intval($result['id']);
    $shift->employeeId = intval($result['employee_id']);
    $shift->dayOfShift = intval($result['day_of_shift']);
    $shift->scheduleNumber = intval($result['schedule_number']);
    $shift->type = $result['type'];

    //parse shift start and end from string to date time
    $shift->shiftStart = DateTime::createFromFormat('H:i:s', $result['shift_start']);
    $shift->shiftEnd = DateTime::createFromFormat('H:i:s',  $result['shift_end']);

    return $shift;
}

function getRawEmployeeShiftsBySchedule(int $scheduleId): array {
    $db = _getConnection();
    $query = "SELECT `id`, `employee_id`, `day_of_shift`, `type`, `shift_start`, `shift_end`, `schedule_number` 
                FROM `raw_employee_shift` WHERE schedule_number=:ScheduleNumber ORDER BY `type`, `day_of_shift`, `shift_start`";

    $statement = $db->prepare($query);
    $statement->bindValue(":ScheduleNumber", $scheduleId);
    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $rawShifts = [];
    foreach ($results as $result) {
        $rawShifts[] = _rawEmployeeShiftFromRow($result);
    }

    return $rawShifts;
}

function insertImperfectShift(ImperfectShift $imperfectShift): int {
    $db = _getConnection();
    $query = "INSERT INTO `imperfect_shift` (`employee_id`, `shift_number`, `timeclock_shift_id`, `shift_date`, `type`, `mins`, `excused`, `reason`, `other`)
     VALUES (:EmployeeId, :ShiftNumber, :TimeClockId, :ShiftDate, :pType, :mins, :excused, :reason, :other)";

    $statement = $db->prepare($query);
    $statement->bindValue(':EmployeeId', $imperfectShift->employeeId);
    $statement->bindValue(':ShiftNumber', $imperfectShift->shiftNumber);
    $statement->bindValue(':TimeClockId', $imperfectShift->timeclockShiftId);
    $statement->bindValue(':ShiftDate', $imperfectShift->shiftDate->format('Y-m-d'));
    $statement->bindValue(':pType', $imperfectShift->type);
    $statement->bindValue(':mins', $imperfectShift->mins);
    $statement->bindValue(':excused', $imperfectShift->excused ? 1 : 0);
    $statement->bindValue(':reason', $imperfectShift->reason);
    $statement->bindValue(':other', $imperfectShift->other);

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}

function _employeeShiftFromRow($result): EmployeeShift {
    $shift = new EmployeeShift();

    $shift->shiftNumber = intval($result['shift_number']);
    $shift->employeeId = intval($result['employee_id']);
    $shift->dayOfShift = intval($result['day_of_shift']);
    $shift->scheduleNumber = intval($result['schedule_number']);

    //parse shift start and end from string to date time
    $shift->shiftStart = date_create_from_format('H:i:s', $result['shift_start']);
    $shift->shiftEnd = date_create_from_format('H:i:s',  $result['shift_end']);

    return $shift;
}

define('EMPLOYEE_SHIFT_QUERY',
    "SELECT `employee_shift`.`shift_number` AS `shift_number`, `employee_id`, `day_of_shift`,`shift_start`, `shift_end`, `schedule_number` 
FROM `employee_shift` ");

function getEmployeeShiftByEmployeeId($employeeId): array {
    $db = _getConnection();
    $query = EMPLOYEE_SHIFT_QUERY . " WHERE `employee_id` = :EmployeeId";

    $statement = $db->prepare($query);
    $statement->bindValue(":EmployeeId", $employeeId);

    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach($results as $result) {
        $parsedResults[] = _employeeShiftFromRow($result);
    }
    return $parsedResults;
}

function getEmployeeShifts($scheduleNumber = 0, $employeeId = 0){
    $db = _getConnection();

    if($employeeId != 0 && $scheduleNumber != 0){//both an employee and schedule were specified
        $query = EMPLOYEE_SHIFT_QUERY . " WHERE `schedule_number` = :ScheduleNum AND `employee_id` = :EmployeeId ORDER BY employee_id, day_of_shift, shift_start";
        $statement = $db->prepare($query);
        $statement->bindValue(":ScheduleNum", $scheduleNumber);
        $statement->bindValue(":EmployeeId", $employeeId);
    }
    else if($employeeId == 0) {//if no employee was selected
        $query = EMPLOYEE_SHIFT_QUERY . " WHERE `schedule_number` = :ScheduleNum ORDER BY employee_id, day_of_shift, shift_start";
        $statement = $db->prepare($query);
        $statement->bindValue(":ScheduleNum", $scheduleNumber);
    }
    else{//if no schedule is selected
        $query = EMPLOYEE_SHIFT_QUERY . " WHERE `employee_id` = :EmployeeId ORDER BY employee_id, day_of_shift, shift_start";
        $statement = $db->prepare($query);
        $statement->bindValue(":EmployeeId", $employeeId);
    }

    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _employeeShiftFromRow($result);
    }
    return $parsedResults;
}

function _imperfectShiftFromRow($result): ImperfectShift {
    $shift = new ImperfectShift();

    $shift->imperfect_shift_number = intval($result['imperfect_shift_number']);
    $shift->employeeId = intval($result['employee_id']);
    $shift->shiftNumber = intval($result['shift_number']);
    $shift->timeclockShiftId = $result['timeclock_shift_id'];
    $shift->type = $result['type'];
    $shift->mins = intval($result['mins']);
    $shift->excused = $result['excused'] > 0;
    $shift->reason = $result['reason'];
    $shift->other = $result['other'];

    //parse shift start and end from string to date time
    $shift->shiftDate = date_create_from_format('Y-m-d', $result['shift_date']);
    return $shift;
}

const SCHEDULE_QUERY = 'SELECT `schedule_number`, `start_active_date`, `end_active_date` FROM `schedule`';

function _scheduleFromRow($result): Schedule {
    $schedule = new Schedule();

    $schedule->scheduleNumber = intval($result['schedule_number']);
    $schedule->startActiveDate = DateTime::createFromFormat('Y-m-d H:i:s', $result['start_active_date']);
    $schedule->endActiveDate = DateTime::createFromFormat('Y-m-d H:i:s', $result['end_active_date']);

    return $schedule;
}

//gets all schedules that are relevant for a particular date range
function getSchedulesByDates(\DateTime $startDate = null, \DateTime $endDate =  null): array {

    if($startDate == null)
        $startDate = DateTime::createFromFormat('Y-m-d', '1995-10-13');
    if($endDate == null)
        $endDate = DateTime::createFromFormat('Y-m-d', '9999-10-13');

    $db = _getConnection();
    $query = SCHEDULE_QUERY . " WHERE end_active_date >= :startDate AND start_active_date <= :endDate ORDER BY start_active_date";
    //convert datetime objects to strings
    $strStartDate = $startDate->format('Y-m-d');
    $strEndDate = $endDate->format('Y-m-d');

    $statement = $db->prepare($query);
    $statement->bindValue(':startDate', $strStartDate);
    $statement->bindValue(':endDate', $strEndDate);

    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach($results as $result){
        $parsedResults[] = _scheduleFromRow($result);
    }
    return $parsedResults;
}

function getActiveScheduleForDate(DateTime $date = null) {
    if (is_null($date))
        $date = new DateTime();

    $db = _getConnection();
    $query = SCHEDULE_QUERY . " WHERE end_active_date >= :Date AND start_active_date <= :Date";

    $statement = $db->prepare($query);
    $statement->bindValue(':Date', date_format($date, 'Y-m-d'));

    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    if ($result)
        return _scheduleFromRow($result);
    else
        return null;
}

function updateSchedule(Schedule $schedule): int {
    $db = _getConnection();

    $query = "UPDATE `schedule` SET `start_active_date`=:StartActiveDate, `end_active_date`=:EndActiveDate 
      WHERE schedule_number=:ScheduleNumber";

    $statement = $db->prepare($query);
    $statement->bindValue(':StartActiveDate', $schedule->startActiveDate->format('Y-m-d'));
    $statement->bindValue(':EndActiveDate', $schedule->endActiveDate->format('Y-m-d'));
    $statement->bindValue(':ScheduleNumber', $schedule->scheduleNumber);

    $statement->execute();
    $statement->closeCursor();

    return $schedule->scheduleNumber;
}

function deleteScheduleById(int $id) {
    $db = _getConnection();

    $query = "DELETE FROM `schedule` WHERE `schedule_number`=:Id";
    $statement = $db->prepare($query);
    $statement->bindValue(':Id', $id);
    $statement->execute();
    $statement->closeCursor();
}


function deleteSchedule(Schedule $schedule) {
    deleteScheduleById($schedule->scheduleNumber);
}

define('IMPERFECT_SHIFT_QUERY',
    "SELECT `imperfect_shift_number`, `employee_id`, `shift_number`, `timeclock_shift_id`, `shift_date`, `type`, `mins`, `excused`, `reason`, `other` 
FROM `imperfect_shift`");

function getImperfectShiftsByEmployee(int $employeeId){
    $db = _getConnection();
    $query = IMPERFECT_SHIFT_QUERY . " WHERE employee_id = :EmployeeId";

    $statement = $db->prepare($query);
    $statement->bindValue(":EmployeeId", $employeeId);

    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _imperfectShiftFromRow($result);
    }
    return $parsedResults;
}

function getImperfectShift(int $imperfectShiftId){
    $db = _getConnection();
    $query = IMPERFECT_SHIFT_QUERY . ' WHERE imperfect_shift_number = :imperfectShiftNumber';

    $statement = $db->prepare($query);
    $statement->bindValue(":imperfectShiftNumber",$imperfectShiftId);

    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    if ($result)
        return _imperfectShiftFromRow($result);

    return null;
}

function getImperfectShiftsByDate(DateTime $startDate, DateTime $endDate = null): array {
    $db = _getConnection();
    $statement= null;
    if(!is_null($endDate)) {
        $query = IMPERFECT_SHIFT_QUERY . " WHERE shift_date >= :startDate AND shift_date <= :endDate";
        $statement = $db->prepare($query);
        $statement->bindValue(":endDate", $endDate->format('Y-m-d'));
    } else {
        $query = IMPERFECT_SHIFT_QUERY . " WHERE shift_date >= :startDate";
        $db->prepare($query);
    }
    $statement->bindValue(":startDate", $startDate->format('Y-m-d'));

    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = array();
    foreach ($results as $result) {
        $parsedResults[] = _imperfectShiftFromRow($result);
    }
    return $parsedResults;
}

function getImperfectShiftByEmployeeAndDate(int $employeeId, DateTime $startDate, DateTime $endDate = null){
    $db = _getConnection();
    if(!is_null($endDate)) {
        $query = IMPERFECT_SHIFT_QUERY . " WHERE shift_date >= :startDate AND shift_date <= :endDate AND employee_id = :employeeId";
        $statement = $db->prepare($query);
        $statement->bindValue(":endDate", $endDate->format('Y-m-d'));
    } else{
        $query = IMPERFECT_SHIFT_QUERY ." WHERE shift_date >= :startDate AND shift_date <= :endDate";
        $statement=$db->prepare($query);
    }

    $statement->bindValue(":startDate", $startDate->format('Y-m-d'));
    $statement->bindValue(":employeeId", $employeeId);
    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = [];
    foreach($results as $result) {
        $parsedResults[] = _imperfectShiftFromRow($result);
    }
    return $parsedResults;
}

function updateImperfectShift(ImperfectShift $imperfectShift): int {
    $db = _getConnection();
    $query = "UPDATE `imperfect_shift` SET `employee_id` = :EmployeeId, `shift_number` = :ShiftNumber, `timeclock_shift_id` = :timeclockId, 
      `shift_date` = :shiftDate, `type` = :pType, `mins` = :mins, `excused` = :excused, `reason` = :reason, `other` = :other
      WHERE `imperfect_shift_number` = :imperfectShiftNumber";

    $statement = $db->prepare($query);
    $statement->bindValue(':imperfectShiftNumber', intval($imperfectShift->imperfect_shift_number));
    $statement->bindValue(':EmployeeId', $imperfectShift->employeeId);
    $statement->bindValue(':ShiftNumber', $imperfectShift->shiftNumber);
    $statement->bindValue(':timeclockId', $imperfectShift->timeclockShiftId);
    $statement->bindValue(':shiftDate', $imperfectShift->shiftDate->format('Y-m-d'));
    $statement->bindValue(':pType', $imperfectShift->type);
    $statement->bindValue(':mins', $imperfectShift->mins);
    $statement->bindValue(':excused', $imperfectShift->excused);
    $statement->bindValue(':reason', $imperfectShift->reason);
    $statement->bindValue(':other', $imperfectShift->other);

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}

function _insertRepeatingReservation(Repeating_Reservation $repeating_lab): int {
    $db = _getConnection();
    $query = "INSERT INTO `repeating_reservation` (start_date,  end_date, faculty_id, monday, tuesday, wednesday, thursday, friday, reserving_class, is_teaching_lab, start_time, end_time) VALUES 
        (:start_date, :end_date, :faculty_id, :mon, :tues, :wed, :thurs, :fri, :reserving_class, :is_teaching_lab, :start_time, :end_time)";

    $statement= $db->prepare($query);
    $statement->bindValue(":start_date", $repeating_lab->start_date->format('Y-m-d'));
    $statement->bindValue(":end_date", $repeating_lab->end_date->format('Y-m-d'));
    if(isset($repeating_lab->faculty_id)) {
        $statement->bindValue(':faculty_id', $repeating_lab->faculty_id);
    } else {
        $statement->bindValue(":faculty_id", $_SESSION['userId']);
    }
    $statement->bindValue(":mon", ($repeating_lab->monday) ? 1:0);
    $statement->bindValue(":tues", ($repeating_lab->tuesday) ? 1:0);
    $statement->bindValue(":wed", ($repeating_lab->wednesday) ? 1:0);
    $statement->bindValue(":thurs", ($repeating_lab->thursday) ? 1:0);
    $statement->bindValue(":fri", ($repeating_lab->friday) ? 1:0);
    $statement->bindValue(":reserving_class", $repeating_lab->reserving_class);
    $statement->bindValue(":is_teaching_lab", ($repeating_lab->is_teaching_lab) ? 1:0);
    $statement->bindValue(":start_time", $repeating_lab->start_time->format("H:i"));
    $statement->bindValue(":end_time", $repeating_lab->end_time->format("H:i"));

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}

function _removeRepeatingReservation($repeating_lab) {
    $db = _getConnection();
    $query = "DELETE FROM `repeating_reservation` WHERE `repeating_reservation_id` = :id";

    $statement = $db->prepare($query);
    $statement->bindValue(":id", $repeating_lab->repeating_res_id);

    $statement->execute();
    $statement->closeCursor();
}

function persistLab($lab): int {
    if (!isset($lab->reservation_id) || $lab->reservation_id == null) {
        return _insertLabReservation($lab);
    } else {
        return _updateLabReservation($lab);
    }
}
function _insertLabReservation(Lab $lab): int {
    $db = _getConnection();

    $query = "INSERT INTO `reservation` (reservation_date, start_time, end_time, department,
    reserving_class, reserving_section,
    reserving_staff, is_teaching_lab, repeating_reservation_id) VALUES 
      (:reservation_date, :start_time, :end_time, :department, :reserving_class, 
      :reserving_section, :reserving_staff, :is_teaching_lab, :repeating_reservation_id)";


    $statement = $db->prepare($query);

    $statement->bindValue(':reservation_date', $lab->date->format('Y-m-d'));
    $statement->bindValue(':start_time', $lab->start_time->format('H:i'));
    $statement->bindValue(':end_time', $lab->end_time->format('H:i'));
    $statement->bindValue(':department', $lab->dept);
    $statement->bindValue(':reserving_class', $lab->reserving_class);
    $statement->bindValue(':reserving_section', $lab->reserving_section);
    $statement->bindValue(':reserving_staff', $lab->faculty_id);
    $statement->bindValue(':is_teaching_lab',  ($lab->is_teaching_lab) ? 1:0);
    if (isset($lab->repeating_reservation_id)) {
        $statement->bindValue(':repeating_reservation_id', $lab->repeating_reservation_id);
    } else {
        $statement->bindValue(':repeating_reservation_id', null, PDO::PARAM_NULL);
    }

    $statement->execute();
    $statement->closeCursor();
    return $db->lastInsertId();
}

function _updateLabReservation(Lab $lab): int {
    $db = _getConnection();
    $query = "UPDATE `reservation` SET `reservation_date`=:reservation_date, `start_time`=:start_time,
      `end_time`=:end_time, `is_teaching_lab`=:is_teaching_lab WHERE `reservation_id`=:Id";

    $statement = $db->prepare($query);
    $statement->bindValue(':reservation_date', $lab->date->format('Y-m-d'));
    $statement->bindValue(':start_time', $lab->start_time->format('H:i'));
    $statement->bindValue(':end_time', $lab->end_time->format('H:i'));
    $statement->bindValue(':is_teaching_lab', ($lab->is_teaching_lab) === 'teaching' ? 1:0);
    $statement->bindValue(":Id", $lab->reservation_id);

    $statement->execute();
    $statement->closeCursor();

    return $lab->reservation_id;
}

function _removeLabReservation(Lab $lab) {
    $db = _getConnection();
    $query = "DELETE FROM reservation WHERE reservation_id = :reservation_id";
    $statement = $db->prepare($query);
    $statement->bindValue(':reservation_id', $lab->reservation_id);

    $statement->execute();
    $statement->closeCursor();
}

function getLabRequestById(int $id) {
    $db = _getConnection();
    $query = "SELECT * FROM `reservation` WHERE `reservation_id`=:Id";

    $statement = $db->prepare($query);
    $statement->bindValue(":Id", $id);
    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    if ($result)
        return _labFromRow($result);

    return null;
}

function checkReservationDates(DateTime $date, DateTime $start_time, DateTime $end_time, bool $is_teaching_lab): bool {
    $db = _getConnection();
    $query = "SELECT COUNT(`reservation_id`) AS `conflicting_reservations` FROM `reservation` WHERE
  `reservation_date` = :date AND  ((
    ( `start_time` <= :start AND :start < `end_time`) OR (`start_time` <= :end AND :end < `end_time` )
  ) OR (
    ( :start  <= `start_time` AND `start_time` < :end) OR ( :start  <= `end_time` AND `end_time` < :end )
  )) AND `is_teaching_lab` = :is_teaching_lab";

    $modifiedStart = clone $start_time;
    /** @noinspection PhpUnhandledExceptionInspection */
    $modifiedStart->add(new DateInterval('PT1M'));
    $statement = $db->prepare($query);
    $statement->bindValue(":date", $date->format('Y-m-d'));
    $statement->bindValue(":start", $modifiedStart->format('H:i'));
    $statement->bindValue(":end", $end_time->format('H:i'));
    $statement->bindValue(":is_teaching_lab", ($is_teaching_lab) ? 1:0);

    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();

    return intval($result['conflicting_reservations']) > 0;
}

function _holidayFromRow($result): Holiday{
    $holiday = new Holiday();

    $holiday->holidayId = $result['holiday_id'];
    $holiday->holidayStartDate = date_create_from_format('Y-m-d',$result['holiday_start_date']);
    $holiday->holidayEndDate = date_create_from_format('Y-m-d',$result['holiday_end_date']);
    $holiday->holidayName = $result['holiday_name'];

    return $holiday;
}

function getHolidays(DateTime $startDate, DateTime $endDate): array {
    if(is_null($startDate)){
        $startDate = date_create_from_format('Y-m-d', '1997-06-24');
    }
    if(is_null($endDate)){
        $endDate = date_create_from_format('Y-m-d', '2999-06-24');
    }
    $db = _getConnection();
    $query = "SELECT `holiday_id`,`holiday_name`, `holiday_start_date`, `holiday_end_date` FROM `holiday`
WHERE `holiday_end_date` <= :EndDate AND `holiday_start_date` >= :StartDate
  OR :StartDate BETWEEN `holiday_start_date` AND `holiday_end_date`
ORDER BY `holiday_start_date`, `holiday_end_date`";

    $statement = $db->prepare($query);
    $statement->bindValue(':StartDate', $startDate->format('Y-m-d'));
    $statement->bindValue(':EndDate', $endDate->format('Y-m-d'));

    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();

    $parsedResults = [];
    foreach($results as $result){
        $parsedResults[] = _holidayFromRow($result);
    }
    return $parsedResults;
}

function _labFromRow($result) {
    $lab = new Lab();

    $lab->reservation_id = $result['reservation_id'];
    $lab->faculty_id = $result['reserving_staff'];
    $lab->dept = $result['department'];
    $lab->reserving_class = $result['reserving_class'];
    $lab->reserving_section = $result['reserving_section'];
    $lab->is_teaching_lab = $result['is_teaching_lab'];
    $lab->date = DateTime::createFromFormat('Y-m-d', $result['reservation_date']);
    $lab->start_time = DateTime::createFromFormat('H:i:s', $result['start_time']);
    $lab->end_time = DateTime::createFromFormat('H:i:s', $result['end_time']);
    $lab->repeating_reservation_id = $result['repeating_reservation_id'];
    return $lab;
}

function getReservationsByDateRange(DateTime $startDate, DateTime $endDate) {
    $db = _getConnection();
    $query = "SELECT * from `reservation` WHERE `reservation_date` >= :startDate AND `reservation_date` <= :endDate";

    $statement = $db->prepare($query);
    $statement->bindValue(":startDate", $startDate->format('Y-m-d'));
    $statement->bindValue(":endDate", $endDate->format('Y-m-d'));

    $statement->execute();
    $results = $statement->fetchAll();
    $statement->closeCursor();
    $parsedResults = [];
    foreach ($results as $result) {
        $reservation = new \Lab();
        $reservation->reservation_id = $result['reservation_id'];
        $reservation->date = $result['reservation_date'];
        $reservation->start_time = $result['start_time'];
        $reservation->end_time = $result['end_time'];
        $reservation->dept = $result['department'];
        $reservation->reserving_class = $result['reserving_class'];
        $reservation->reserving_section = $result['reserving_section'];
        $reservation->faculty_id = $result['reserving_staff'];
        $reservation->is_teaching_lab = $result['is_teaching_lab'];

        $event = new \Event();
        $event->title = $reservation->dept . " " . $reservation->reserving_class . "-" . $reservation->reserving_section;
        $event->start = $reservation->date . "T" . $reservation->start_time;
        $event->end = $reservation->date ."T" . $reservation->end_time;
        $event->color = ($reservation->is_teaching_lab==1) ? '#17a2b8':'#28a745';
        $reservation->repeating_reservation_id = $result['repeating_reservation_id'];

        array_push($parsedResults, $event);
    }

    return $parsedResults;
}

function _getLabReservationsWithReservingFaculty(int $userId){
    $db = _getConnection();

    if(isFsr()) {
        $query = "SELECT * FROM `reservation` INNER JOIN `users` ON users.id = reservation.reserving_staff WHERE reservation_date > NOW() ORDER BY `reservation_date` ASC,`start_time` ASC, `end_time` ASC";

    }
    else {
        $query = "SELECT * FROM `reservation` INNER JOIN `users` ON users.id = reservation.reserving_staff WHERE users.id = :id AND reservation_date > NOW() ORDER BY `reservation_date` ASC,`start_time` ASC, `end_time` ASC";
    }
    $statement = $db->prepare($query);
    $statement->bindValue(":id", $userId);

    $statement->execute();
    $results = $statement->fetchAll();
    $parsedResults = [];
    foreach ($results as $lab) {
        $res = new \Lab();

        $res->reservation_id = $lab['reservation_id'];
        $res->date = $lab['reservation_date'];
        $res->start_time = DateTime::createFromFormat('H:i:s',$lab['start_time']);
        $res->end_time = DateTime::createFromFormat('H:i:s', $lab['end_time']);
        $res->dept = $lab['department'];
        $res->reserving_class = $lab['reserving_class'];
        $res->reserving_section = $lab['reserving_section'];
        $res->faculty_id = $lab['reserving_staff'];
        $res->faculty_username = $lab['username'];
        $res->is_teaching_lab = $lab['is_teaching_lab'];
        $res->repeating_reservation_id = $lab['repeating_reservation_id'];

        array_push($parsedResults, $res);
        unset($res);
    }
    return $parsedResults;
}

function _getRepeatingReservationsWithReservingFaculty(int $userId) {
    $db = _getConnection();
    if(isFsr()) {
        $query = "SELECT * FROM `repeating_reservation` INNER JOIN `users` ON users.id = repeating_reservation.faculty_id WHERE (start_date > NOW() OR end_date > NOW()) ORDER BY `start_date` ASC, `start_time` ASC, `end_time` ASC";
    }
    else {
        $query = "SELECT * FROM `repeating_reservation` INNER JOIN `users` ON users.id = repeating_reservation.faculty_id WHERE users.id = :id AND (start_date > NOW() OR end_date > NOW()) ORDER BY `start_date` ASC, `start_time` ASC, `end_time` ASC";
    }
    $statement = $db->prepare($query);
    $statement->bindValue(":id", $userId);

    $statement->execute();
    $results = $statement->fetchAll();
    $parsedResults=[];
    foreach ($results as $res) {
        array_push($parsedResults, _repeatingReservationFromRow($res));
    }
    $statement->closeCursor();
    return $parsedResults;
}

function getRepeatingReservationById($id) {
    $db = _getConnection();
    $query = "SELECT * FROM `repeating_reservation` WHERE `repeating_reservation_id` = :id";
    $statement = $db->prepare($query);
    $statement->bindValue(":id", $id);
    $statement->execute();

    $result = $statement->fetch();
    if ($result)
        return _repeatingReservationFromRow($result);

    return null;
}

function _repeatingReservationFromRow($result) {
    $repeating_res = new \RepeatingReservationOutput();
    if(isset($result['username'])) {
        $repeating_res->reserving_faculty_username = $result['username'];
    }
    $repeating_res->reserving_faculty_id = $result['faculty_id'];
    $repeating_res->reserving_class = $result['reserving_class'];
    $repeating_res->start_date = $result['start_date'];
    $repeating_res->end_date = $result['end_date'];
    $repeating_res->repeating_res_id = $result['repeating_reservation_id'];
    $repeating_res->is_teaching_lab = $result['is_teaching_lab'];
    $repeating_res->start_time = DateTime::createFromFormat('H:i:s',$result['start_time']);
    $repeating_res->end_time = DateTime::createFromFormat('H:i:s', $result['end_time']);
    $repeating_res->days_string = (($result['monday']==1) ? "M":"") . (($result['tuesday']==1) ? "T":"") .(($result['wednesday']==1) ? "W":"") .(($result['thursday']==1) ? "Th":"") .(($result['friday']==1) ? "F":"");
    return $repeating_res;
}

function removeHoliday($holidayId){
    $db = _getConnection();

    $query = "DELETE FROM holiday WHERE holiday_id = :holidayId";
    $statement = $db->prepare($query);
    $statement->bindValue(':holidayId', $holidayId);

    $statement->execute();
    $statement->closeCursor();
}

function _imperfectShiftInfoFromRow($result){
    $shift = new \ImperfectShiftInformation();

    $shift->employeeId = $result['employee_id'];
    $shift->shiftDate = date_create_from_format('Y-m-d',$result['shift_date']);
    $shift->shiftTime = $result['shift_start'];
    $shift->shiftEnd = $result['shift_end'];
    $shift->type = $result['type'];
    $shift->mins = $result['mins'];
    $shift->excused = $result['excused'] === '1';
    $shift->other = $result['other'];
    $shift->reason = $result['reason'];
    $shift->shiftNumber = $result['shift_number'];
    $shift->timeclockShiftId = $result['timeclock_shift_id'];
    $shift->imperfect_shift_number = $result['imperfect_shift_number'];

    return $shift;
}

function getImperfectShiftInfo(int $employeeId = 0, DateTime $startDate = null, DateTime $endDate = null){
    $db = _getConnection();
    $query = 'SELECT imperfect_shift.employee_id, `shift_date`, `shift_start`, `shift_end`, `type`, `mins`, `excused`, `reason`, `other`, `imperfect_shift_number`,
imperfect_shift.shift_number, `timeclock_shift_id`
 FROM imperfect_shift INNER JOIN
  employee_shift es ON imperfect_shift.shift_number = es.shift_number
  WHERE shift_date > :startDate AND shift_date < :endDate';


    if($startDate == null)
        $startDate = date_create_from_format('Y-m-d', '1995-10-13');
    if($endDate == null)
        $endDate = date_create_from_format('Y-m-d', '9999-10-13');
    if($employeeId != 0){
        $query = $query . ' AND imperfect_shift.employee_id = :employeeId ORDER BY imperfect_shift.employee_id, `shift_date`';
        $statement = $db->prepare($query);
        $statement->bindValue(':employeeId', $employeeId);
    }else{
        $query = $query. " ORDER BY imperfect_shift.employee_id, `shift_date`";
        $statement = $db->prepare($query);
    }

    $statement->bindValue(':startDate', $startDate->format('Y-m-d'));
    $statement->bindValue(':endDate', $endDate->format('Y-m-d'));

    $statement->execute();
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    $statement->closeCursor();
    $parsedResults = [];
    foreach($results as $result){
        $parsedResults[] = _imperfectShiftInfoFromRow($result);
    }
    return $parsedResults;
}
function getLastQueriedDate(): DateTime {
    $db = _getConnection();
    $query = 'SELECT `id`, `query_date` FROM `last_queried_date` WHERE `id` = 1';
    $statement = $db->prepare($query);
    $statement->execute();
    $result = $statement->fetch();
    $statement->closeCursor();
    $lastQueriedDate = DateTime::createFromFormat('Y-m-d', $result['query_date']);
    return $lastQueriedDate;
}

function updateLastQueriedDate(DateTime $lastQueriedDate){
    $db = _getConnection();
    $query = "INSERT INTO `last_queried_date` (`id`, `query_date`) VALUES (1, :queryDate) ON DUPLICATE KEY UPDATE `query_date` = :queryDate";
    $statement = $db->prepare($query);

    $statement->bindValue(':queryDate', $lastQueriedDate->format('Y-m-d'));

    $statement->execute();
    $statement->closeCursor();
}
