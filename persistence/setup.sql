CREATE TABLE `users` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `username` VARCHAR(200) UNIQUE NOT NULL,
  `host` VARCHAR(100) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARBINARY(255),
  `salt` VARBINARY(255),
  `is_active` TINYINT NOT NULL DEFAULT 1,
  `role` VARCHAR(100) NOT NULL DEFAULT 'faculty'
);

-- Insert Initial Users
-- Be Sure To Change The Password
SET @SALT = SUBSTRING(MD5(RAND()), -10);
INSERT INTO `users` (username, host, `email`, password, salt, is_active, role)
VALUES ('fsradmin', 'localhost', 'beckerlab@clarion.edu', SHA2(concat(@SALT, 'password'), 512), @SALT, 1, 'fsr') ,
  ('beckerlab', 'clarion.edu', 'beckerlab@clarion.edu', NULL, NULL, 1, 'fsr'),
  ('skim', 'clarion.edu', 'skim@clarion.edu', NULL, NULL, 1, 'fsr');

-- Table Stores The Validators & Expiry Dates
-- For Auth Cookies.
-- One User May Have Several Auth Cookies Active At Once
CREATE TABLE `auth_cookie` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `authenticator` VARBINARY(255) NOT NULL,
  `expires` DATE NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  INDEX(`authenticator`),
  INDEX(`expires`)
);

CREATE TABLE `request` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `created` DATETIME NOT NULL,
  `class` VARCHAR(255) NOT NULL,
  `drives` INT NOT NULL DEFAULT 0,
  `operating_system` VARCHAR(255) NOT NULL,
  `other` TEXT NOT NULL,
  `status` VARCHAR(200) NOT NULL DEFAULT 'Open',
  INDEX(`user_id`),
  INDEX (`status`),
  FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

-- Create a table that maps between day names and day numbers*/
CREATE TABLE `weekdays`(
  `number`	INT,
  `name`	VARCHAR(10) NOT NULL,
  CONSTRAINT `weekdays_pk` PRIMARY KEY(`number`)
);

-- Insert day names into weekdays table
INSERT INTO `weekdays`(`number`, `name`)
VALUES	(0, 'Sunday'),
  (1, 'Monday'),
  (2, 'Tuesday'),
  (3, 'Wednesday'),
  (4, 'Thursday'),
  (5, 'Friday'),
  (6, 'Saturday');

-- Table that maps shifts to the appropriate active start
-- date and end data
CREATE TABLE `schedule`(
  `schedule_number`	INT AUTO_INCREMENT,
  `start_active_date` DATETIME NOT NULL,
  `end_active_date`		DATETIME NOT NULL,
  CONSTRAINT `schedule_pk` PRIMARY KEY(`schedule_number`)
);
CREATE INDEX `schedule_idx` ON `schedule`(`start_active_date`, `end_active_date`);


-- Table that represents a shift that an employee is supposed to work
-- Each shift will be associated with a schedule, this schedule provides
-- the start and end date of when the shift becomes active
CREATE TABLE `employee_shift`(
  `shift_number`	  INT AUTO_INCREMENT,
  `employee_id`		  INT NOT NULL,
  `day_of_shift`		  INT NOT NULL,
  `shift_start`	  TIME NOT NULL,
  `shift_end`		    TIME NOT NULL,
  `schedule_number`	INT NOT NULL,
  CONSTRAINT `shift_pk` PRIMARY KEY(`shift_number`),
  CONSTRAINT `schedule_shift_fk` FOREIGN KEY (schedule_number) REFERENCES `schedule`(`schedule_number`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `employee_shift_weekday` FOREIGN KEY (`day_of_shift`) REFERENCES  `weekdays`(`number`)
);
CREATE INDEX `employee_idx` ON `employee_shift`(`employee_id`);

-- Table of records directly mimicking the input on the schedule editor.
-- In this table, two back to back shifts are entered as two separate records.
-- As opposed to employee_shift which enters it as one record.
-- This makes reproducing the schedule much easier.
-- Whenever a new schedule is created, this table and employee_shift should both be
-- populated. This table should not be referenced for business logic
CREATE TABLE `raw_employee_shift` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `employee_id` INT NOT NULL,
  `day_of_shift` INT NOT NULL,
  `type`  VARCHAR(30) NOT NULL DEFAULT 'LAB',
  `shift_start`	  TIME NOT NULL,
  `shift_end`		    TIME NOT NULL,
  `schedule_number`	INT NOT NULL,
  FOREIGN KEY (`schedule_number`) REFERENCES `schedule`(`schedule_number`)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  FOREIGN KEY (`day_of_shift`) REFERENCES `weekdays`(`number`)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
CREATE INDEX `raw_employee_shift_type_idx` ON `raw_employee_shift`(`type`);

CREATE TABLE `imperfect_shift`(
  `imperfect_shift_number` INT AUTO_INCREMENT,
  `employee_id`		INT NOT NULL,
  `shift_number`	INT NOT NULL,
  `timeclock_shift_id` INT DEFAULT NULL,
  `shift_date`		DATE,
  `type`          VARCHAR(20) NOT NULL,  /*Missed, Late, Early, etc.*/
  `mins`		      INT,          /*Can be null if shift was missed completely*/
  `excused`       TINYINT(1) NOT NULL DEFAULT 0,
  `reason`        VARCHAR(255),
  `other`         TEXT,
  CONSTRAINT `imperfect_shift_pk` PRIMARY KEY(`imperfect_shift_number`),
  CONSTRAINT `imperfect_shift_fk` FOREIGN KEY(`shift_number`) REFERENCES `employee_shift`(`shift_number`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE INDEX `shift_date_idx` ON `imperfect_shift`(`shift_date`);
CREATE INDEX `employee_id_idx` ON `imperfect_shift`(`employee_id`);

CREATE TABLE `repeating_reservation` (
  `repeating_reservation_id`  INT NOT NULL AUTO_INCREMENT,
  `start_date`    DATE NOT NULL,
  `end_date`      DATE NOT NULL,
  `faculty_id`    INT,
  `monday`        TINYINT(1), # 1 if repeating on this day, 0 otherwise
  `tuesday`       TINYINT(1), # 1 if repeating on this day, 0 otherwise
  `wednesday`     TINYINT(1), # 1 if repeating on this day, 0 otherwise
  `thursday`      TINYINT(1), # 1 if repeating on this day, 0 otherwise
  `friday`        TINYINT(1), # 1 if repeating on this day, 0 otherwise
  `reserving_class` VARCHAR(50),
  `is_teaching_lab` TINYINT(1),
  `start_time`    TIME,
  `end_time`      TIME,
  PRIMARY KEY (`repeating_reservation_id`)
);


CREATE INDEX `start_date_idx` ON `repeating_reservation`(`start_date`);
CREATE INDEX `end_date_idx` ON `repeating_reservation`(`end_date`);

-- table that stores reservation dates
-- Each reservation will correspond to an entry in the database
-- repeating reservations will point reference the repeating reservations table
CREATE TABLE `reservation` (
  `reservation_id`          INT NOT NULL AUTO_INCREMENT,
  `reservation_date`        DATE NOT NULL,
  `start_time`              TIME NOT NULL,
  `end_time`                TIME NOT NULL,
  `department`              VARCHAR(5),
  `reserving_class`         VARCHAR(25) NOT NULL,
  `reserving_section`       VARCHAR(4),
  `reserving_staff`         INT NOT NULL,
  `is_teaching_lab`         TINYINT(1),
  `repeating_reservation_id` INT,
  PRIMARY KEY (`reservation_id`),
  FOREIGN KEY (`reserving_staff`) REFERENCES `users`(`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  FOREIGN KEY (`repeating_reservation_id`) REFERENCES `repeating_reservation`(`repeating_reservation_id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
CREATE INDEX `reservation_date_idx` ON `reservation`(`reservation_date`);

CREATE TABLE `holiday` (
  `holiday_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `holiday_name` VARCHAR(100) NOT NULL,
  `holiday_start_date` DATE NOT NULL,
  `holiday_end_date`   DATE NOT NULL
);
CREATE INDEX `holiday_date_idx` ON `holiday`(`holiday_start_date`, `holiday_end_date`);

CREATE TABLE `last_queried_date` (
  `id` INT PRIMARY KEY,
  `query_date` DATE NOT NULL,
  INDEX (`query_date`)
);

INSERT INTO `last_queried_date` (`id`, `query_date`) VALUES (1, '1997-06-24');
