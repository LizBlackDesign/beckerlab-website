<?php
namespace timeclock;

class Configuration {
    /**
     * Base URL for the TimeClock REST API
     * Should Not End With a Trailing /
     */
    const API_BASE_URL = 'https://209.250.209.120:5001';

    /**
     * A Valid Readonly Key For The Timeclock API
     */
    const API_KEY = '_rope%hulu _-queen3*TOKYO3visa%USA6coffee';
}

class Response {
    /**
     * @var string cURL's message from the status code
     */
    public $curlMessage;

    /**
     * @var int cURL's Reported Status Code (See: <a href="https://curl.haxx.se/libcurl/c/libcurl-errors.html">libcurl's Documentation</a>)
     */
    public $curlStatusCode;

    /**
     * @var int HTTP Response Code
     */
    public $httpStatusCode;

    /**
     * @var object|null Data returned from the request. null if $curlStatusCode indicates an error (not CURLE_OK)
     */
    public $data;
}

class Employee {
    /**
     * @var \DateTime
     */
    public $dateHire;

    /**
     * @var null|\DateTime
     */
    public $dateLeft;

    /**
     * @var int
     */
    public $employeeId;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;
}

class Punch {
    /**
     * @var int
     */
    public $employeeId;

    /**
     * @var int
     */
    public $recordId;

    /**
     * @var \DateTime
     */
    public $timeIn;

    /**
     * @var \DateTime
     */
    public $timeOut;
}

/**
 * Creates A cURL Handle With All Base Options
 * Sets The API Key
 *
 * @param string $url
 * The URL To Append To The TIMECLOCK_API_BASE_URL
 * Should Start With A /
 *
 * @return resource A cURL Handle For Connecting To The TimeClock Database
 *
 * @example $ch = _makeConnection('/employee');
 */
function _makeConnection(string $url) {
    $ch = curl_init(Configuration::API_BASE_URL . $url);

    // Return Results Directly From curl_exec Rather Than Having To Retrieve Them
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        // Our API Key, See: https://gitlab.com/clarion-fsr/timeclock-http-bridge
        'Authorization: Token ' . Configuration::API_KEY
    ));

    // The Department Server's SSL Cert Is Self Signed
    // So, It won't verify
    // This allows cURL To Accept It
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    return $ch;
}

/**
 * Executes The Request In The cURL Handle, Packages And Returns The Results
 *
 * @param $curlHandle
 * The cURL Handle To Run
 * Will Be Closed At The End of This Function If $closeHandle Is True
 *
 * @param bool $closeHandle
 * Default: true <br />
 * Weather Or Not The cUrl Handle Should Be Closed
 * At The End of This Function
 *
 *
 * @return array
 * Associative Array With The Given Structure <br />
 * ['statusMessage'] cURL's Reported Message <br />
 * ['statusCode'] cURL's Reported Status Code (See: <a href="https://curl.haxx.se/libcurl/c/libcurl-errors.html">libcurl's Documentation</a>) <br />
 * ['httpCode'] HTTP response code from the cURL request
 * ['data'] Set Only If cURL Reports No Errors! The Response From The TimeClock Server
 */
function _executeRequest($curlHandle, bool $closeHandle = true) {
    // Execute Request And Pull Results From cURL
    // If No Content Is Returned To cURL
    // 'NULL' is The Result
    $curlResult = curl_exec($curlHandle);

    // Package Results
    $result = [
        'statusMessage' => curl_error($curlHandle),
        'statusCode' => curl_errno($curlHandle),
        'httpCode' =>  curl_getinfo($curlHandle, CURLINFO_HTTP_CODE)
    ];

    // If We Succeeded, Write Data \
    if (curl_errno($curlHandle) === CURLE_OK)
        $result['data'] = json_decode($curlResult);

    if ($closeHandle)
        curl_close($curlHandle);

    return $result;
}

/**
 * Gets Current Employees As A JSON Array
 *
 * @param string $department
 * Default: 'CIS DEPT'
 * Department To Retrieve Employees From
 *
 * @param string $activeOn
 * Set to retrieve employees with termination dates (DateLeft) after this value,
 * or no termination date at all. (i.e. Employees active on, and after this date)
 * formatted mm-dd-yyyy Inclusive
 * Default:'10-19-1995'
 *
 * @return Response
 */
function getEmployees(string $department = 'CIS DEPT', string $activeOn = '10-19-1995'): Response {
    $res = _executeRequest(_makeConnection('/employee?' . http_build_query(['department' => $department, 'active_on' => $activeOn])));

    $response = new Response();
    $response->curlMessage = $res['statusMessage'];
    $response->curlStatusCode = $res['statusCode'];
    $response->httpStatusCode = $res['httpCode'];

    if (isset($res['data'])) {
        $response->data = array_map(function ($value) {
            $employee = new Employee();
            $employee->dateHire = date_create_from_format('m-d-Y H:i:s', $value->DateHire);
            if (isset($value->DateLeft) && !is_null($value->DateLeft)) {
                $employee->dateLeft = date_create_from_format('m-d-Y H:i:s', $value->DateLeft);
            }
            $employee->employeeId = $value->EmployeeId;
            $employee->firstName = $value->FirstName;
            $employee->lastName = $value->LastName;
            return $employee;
        }, $res['data']);
    }

    return $response;

}

/**
 * Gets A Given Employee's Punches
 *
 * @param int $employeeId
 * Optional, If Unspecified, Then All Punches In The Range Will Be Returned
 * ID of The Employee
 *
 * @param \DateTime $startDate
 * Default: 10-19-1995 <br />
 * Date To Retrieve Punches After
 *
 * @param \DateTime $endDate
 * Default: 10-19-9999 <br />
 * Date To Retrieve Punches Before
 *
 * @return Response
 */
function getEmployeePunches(int $employeeId = 0, \DateTime $startDate = null, \DateTime $endDate = null): Response {

    if (is_null($startDate))
        $startDate = date_create_from_format('m-d-Y', '10-19-1995');

    if (is_null($endDate))
        $endDate = date_create_from_format('m-d-Y', '10-19-9999');

    $params = [
        'start' => $startDate->format('m-d-Y'),
        'end' => $endDate->format('m-d-Y')
    ];

    // Only attach employee ID if it was specified
    if ($employeeId > 0)
        $params['employee_id'] = $employeeId;

    $res = _executeRequest(_makeConnection('/punch?' . http_build_query($params)));

    $response = new Response();
    $response->curlMessage = $res['statusMessage'];
    $response->curlStatusCode = $res['statusCode'];
    $response->httpStatusCode = $res['httpCode'];

    if (isset($res['data'])) {
        $response->data = array_map(function ($value) {
            $punch = new Punch();
            $punch->employeeId = $value->EmployeeId;
            $punch->recordId = $value->RecordId;
            $punch->timeIn = date_create_from_format('m-d-Y H:i:s', $value->TimeIn);
            $punch->timeOut = date_create_from_format('m-d-Y H:i:s', $value->TimeOut);

            return $punch;
        }, $res['data']);
    }
    return $response;
}
