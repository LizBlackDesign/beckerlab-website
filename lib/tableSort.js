//sort
//params: integer: represents the table heading that has been clicked
//sorts the table in ascending or descending order based on the
//heading that was clicked. If there were no switches, and the order was asc,
//then table will be sorted in descending order by the clicked table heading
function sort(n, tableName) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableName);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount ++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }

    var tableSel = $('#' + tableName);
    tableSel.find('tr.visible-employee').each(function (index) {
        $(this).toggleClass("stripe", !!(index & 1));
    });

    var headers = tableSel.find('thead tr th');
    headers.find('div').remove();

    var header = headers.eq(n);
    if (dir === 'asc')
        header.append('<div style="display: inline"><i class="fas fa-caret-up"></i></div>');
    else
        header.append('<div style="display: inline"><i class="fas fa-caret-down"></i></div>');
}


//sorts dates
function sortDate(n, tableName){

    //converts the date to an 8 digit string
    //if employee is still active, just returns a blank string to bring
    //those employees to the top (asc) or bottom (desc)
    function convertDate(d) {
        //this gets forced to lower case before being passed into the function
        if(d === "still active"){
            return '';
        }
        var p = d.split("/");
        return +(p[2]+p[1]+p[0]);
    }

    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableName);
    switching = true;
    // Set the sorting direction to ascending:
    dir = "asc";
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /* Check if the two rows should switch place,
            based on the direction, asc or desc: */
            if (dir == "asc") {
                if (convertDate(x.innerHTML.toLowerCase()) > convertDate(y.innerHTML.toLowerCase())) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (convertDate(x.innerHTML.toLowerCase()) < convertDate(y.innerHTML.toLowerCase())) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            // Each time a switch is done, increase this count by 1:
            switchcount ++;
        } else {
            /* If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again. */
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }

    var tableSel = $('#' + tableName);
    tableSel.find('tr.visible-employee').each(function (index) {
        $(this).toggleClass("stripe", !!(index & 1));
    });

    var headers = tableSel.find('thead tr th');
    headers.find('div').remove();

    var header = headers.eq(n);
    if (dir === 'asc')
        header.append('<div style="display: inline"><i class="fas fa-caret-up"></i></div>');
    else
        header.append('<div style="display: inline"><i class="fas fa-caret-down"></i></div>');

    $('#' + tableName +  ' tr.visible-employee').each(function (index) {
        $(this).toggleClass("stripe", !!(index & 1));
    })

}