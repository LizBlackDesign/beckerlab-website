<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Holidays</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <script>
        $(document).ready(function() {
            var deleting = false;
            $( "#startDate").datepicker({
                minDate: 0
            });
            $( "#endDate").datepicker({
                minDate: 0
            });
                var holidayRows = $("#holidays").find("tbody").find('tr');
                for (var i = 0; i < holidayRows.length; i++) {
                    var item = $(holidayRows[i]);
                    item.find('td i').parent().click(function (e) {
                        var element = $(e.target).closest('td');
                        var data = {
                            holidayId: element.data("id")
                        };
                        $.ajax({
                            type: 'POST',
                            url: "<?=$config['webRoot']?>index.php?path=/api/holiday/remove",
                            dataType: 'json',
                            statusCode: {
                                401: status401Handler
                            },
                            data: data
                        }).done(function (data, textStatus, jqXHR) {
                            element.closest('tr').remove();
                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            baseAjaxErrorHandler(jqXHR, "Failed to remove holiday");
                        });
                    })
                }

            var form = $("#holidayForm");
            form.on('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();
                var holidayName = $("#forName").val();
                var startDate = $("#startDate").val();
                var endDate = $("#endDate").val();
                var dStartDate = moment($("#startDate").val(), 'MM/DD/YYYY');
                var dEndDate = moment($("#endDate").val(), 'MM/DD/YYYY');

                if (dEndDate.diff(dStartDate, 'days') < 0){
                    alert("End date can not be after start date. Please correct issue and try again.");
                }
                else {
                    var data = {
                        holidayName: holidayName,
                        startDate: startDate,
                        endDate: endDate
                    };

                    $.ajax({
                        type: 'POST',
                        url: "<?=$config['webRoot']?>index.php?path=/api/holiday/add",
                        dataType: 'json',
                        statusCode: {
                            401: function () {
                                window.location.href = "<?=$config['webRoot']?>index.php?path=/login";
                            }
                        },
                        data: data
                    }).done(function (data, textStatus, jqXHR) {
                        /*var id = data['holidayId'];
                        var tableString = "<tr>\n" +
                            " <td>" + holidayName + "</td>\n" +
                            " <td>" + startDate +"</td>\n" +
                            " <td>" + endDate +"</td>\n" +
                            "<td  data-id = \"" + id + "\"><i class=\"fas fa-trash\"></i></td>\n" +
                            "</tr>";

                        $("#holidays").append(tableString);*/
                        location.reload(true);

                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        baseAjaxErrorHandler(jqXHR);
                    })
                }
            })

        });



    </script>

</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container">
    <h1>Holidays</h1>
    <div class="row">
        <div class="col-lg-5 col-sm-12">
            <form id = "holidayForm" class="form-group justify-content-center">
                <div>
                    <h3 class="form-group row titleCentering">Add Holiday</h3>
                    <label for="forName">Holiday Name</label>
                    <input id="forName" type="text" autocomplete="off" class="form-control" placeholder="Spring Break" required>

                    <label for="startDate">Start Date</label>
                    <input id="startDate" autocomplete="off" placeholder="Start Date" class="form-control" type="text" required>

                    <label for="endDate">End Date</label>
                    <input id="endDate" autocomplete="off" placeholder="End Date" class="form-control" type="text" required>


                    <button class="btn btn-lg btn-primary titleCentering" style="margin-top: 5px"  type="submit">Add Holiday</button>
                </div>
            </form>
        </div>
        <div class="col-lg-6 col-sm-12">
            <table id="holidays" class="table table-striped table-sm table-responsive-sm">
                <thead>
                <tr>
                    <th scope="col">Holiday</th>
                    <th scope="col">Start</th>
                    <th scope="col">End</th>
                    <th scope="col">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    $holidays = getHolidays(new DateTime(),
                        date_create_from_format('Y-m-d', '2999-06-24'));
                    foreach($holidays as $holiday) {?>
                        <tr>
                            <td><?= $holiday->holidayName ?></td>
                            <td><?= $holiday->holidayStartDate->format('Y-m-d') ?></td>
                            <td><?= $holiday->holidayEndDate->format('Y-m-d') ?></td>
                            <td data-id = "<?= $holiday->holidayId?>"><i class="fas fa-trash"></i></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>