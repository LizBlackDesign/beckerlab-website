<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Employment Records</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>
    <script src = "<?=$config['webRoot']?>lib/tableSort.js"></script>

    <style>
        .stripe {
            background-color: rgba(0,0,0,.05)
        }
    </style>

    <script>
        $(document).ready(function () {
            var rows = $("#employeeTableBody").find('tr');

            $("#forNameSearch").bind('input', function (e) {
                var str = $(e.target).val().trim().toLowerCase();
                var splitStr = str.split(' ');

                for (var i = 0; i < rows.length; i++) {
                    var item = $(rows[i]);
                    var firstName = item.data('first-name');
                    var lastName = item.data('last-name');

                    // If We Have An Empty String, Match All
                    if (str.length === 0) {
                        item.show();
                        item.addClass('visible-employee');
                        continue;
                    }

                    var containStr = false;
                    for (var j = 0; j < splitStr.length; j++) {
                        var s = splitStr[j];
                        if (firstName.toLowerCase().indexOf(s) !== -1 || lastName.toLowerCase().indexOf(s) !== -1 ) {
                            containStr = true;
                            break;
                        }
                    }

                    if (containStr) {
                        item.show();
                        item.addClass('visible-employee')
                    } else {
                        item.hide();
                        item.removeClass('visible-employee')
                    }
                }

                $('#employeeTable tr.visible-employee').each(function (index) {
                    $(this).toggleClass("stripe", !!(index & 1));
                })
            });
        })
    </script>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container">
    <h1>Employee Records</h1>
    <form id="performanceRequest" novalidate>
        <div class="row">
            <div class="form-group col-lg-3 col-sm-12">
                <input list="employees" id="forNameSearch" type="text" autocomplete="off" class="form-control" placeholder="Type Name"/>
                <datalist id="employees"></datalist>
            </div>
        </div>
    </form>
    <div class="pre-scrollable form-group">
        <table class="table table-sm table-responsive-sm" id="employeeTable">
            <thead>
            <tr>
                <th class = "hoverClick" scope="col" onclick = "sort(0, 'employeeTable')">First Name</th>
                <th class = "hoverClick" scope="col" onclick = "sort(1, 'employeeTable')">Last Name</th>
                <th class = "hoverClick" scope="col" onclick = "sortDate(2, 'employeeTable')">Start</th>
                <th class = "hoverClick" scope="col" onclick = "sortDate(3, 'employeeTable')">End</th>
            </tr>
            </thead>
            <tbody id="employeeTableBody">
            <?php
            $employeeResp = timeclock\getEmployees();
            if (isset($employeeResp->data)) {
                $acc = '';
                $index = 0;
                foreach ($employeeResp->data as $employee) {
                    // Manually Stripe The Table
                    if ($index++ % 2 === 0)
                        $stripeClass = ' stripe ';
                    else
                        $stripeClass = '';
                    $acc .= '<tr class="visible-employee' . $stripeClass . '" data-first-name="' . strtolower($employee->firstName) . '"' . ' data-last-name="' . strtolower($employee->lastName) . '">';
                    $acc .= '<td class="firstName">' . $employee->firstName. '</td>';
                    $acc .= '<td class="lastName">' . $employee->lastName. '</td>';
                    $acc .= '<td>' . $employee->dateHire->format('m/d/Y'). '</td>';
                    $acc .= '<td>' . ((is_null($employee->dateLeft)) ? 'Still Active':$employee->dateLeft->format('m/d/Y')) . '</td>';
                    $acc .= '</tr>';
                }
                echo $acc;
            }
            ?>
            </tbody>
        </table>
    </div>

</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>
