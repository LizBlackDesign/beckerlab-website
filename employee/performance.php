<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Employee Performance</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>
    <script>

        var searchTimeout;
        var table;
        var employees;

        var inputEmployeeName;
        var inputStartDate;
        var inputEndDate;
        var loader;

        function searchFieldKeydown() {

            // If The Field has been altered again before the timeout
            // Reset the timer
            if (searchTimeout)
                clearTimeout(searchTimeout);

            searchTimeout = setTimeout(function () {
                var data = {};
                var employee = employeeFromName(inputEmployeeName.val(), employees);
                if (employee) {
                    data.employeeId = employee.employeeId;
                }

                var startDate = moment(inputStartDate.val(), "MM/DD/YYYY");
                if (startDate.isValid()) {
                    data.startDate = startDate.format('YYYY-MM-DD');
                }

                var endDate = moment(inputEndDate.val(), "MM/DD/YYYY");
                if (endDate.isValid()) {
                    data.endDate = endDate.format('YYYY-MM-DD');
                }
                loader.show();
                performPerformanceSearch(data);
            }, 1000);
        }

        function performPerformanceSearch(data) {
            table.empty();
            loader.show();
            $.ajax({
                type:'GET',
                url:'<?=$config['webRoot']?>index.php?path=/api/performanceQuery',
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401)
                    return;
                alert("Failed query the database for imperfect shifts.");
            });
            $.ajax({
                type:'GET',
                url:'<?=$config['webRoot']?>index.php?path=/api/imperfectshift',
                dataType:'json',
                cache: false,
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
                var acc = "";

                for (var i = 0; i < data.length; i++) {
                    var shift = data[i];
                    var shiftDate = moment(shift.shiftDate);
                    acc += "<tr data-imperfectShiftId = " + shift.imperfect_shift_number + ">";
                    acc += "<td>" + shift.firstName + "</td>";
                    acc += "<td>" + shift.lastName + "</td>";
                    acc += "<td>" + moment(shift.shiftDate.date).format('M/D/Y') + "</td>";
                    acc += "<td>" + shift.shiftTime + "</td>";
                    acc += "<td>" + shift.shiftEnd + "</td>";
                    acc += "<td>" + shift.type + "</td>";
                    if(shift.mins != null)
                        acc += "<td>" + shift.mins + "</td>";
                    else
                        acc += "<td></td>";

                    acc+= "<td><select class=\"custom-select excused\">";

                    if (shift.excused) {
                        acc += '<option value="selected">Excused</option><option value="">Not Excused</option>';
                        acc += "</select></td>";
                        acc += "<td><select class=\"custom-select reason\">";
                    } else {
                        acc += "<option value=\"selected\">Not Excused</option><option value=\"\">Excused</option>";
                        acc += "</select></td>";
                        acc += "<td><select disabled class=\"custom-select reason\">";
                    }




                    if(shift.reason === "Other") {
                        acc += '<option value="na">N/A</option>';
                        acc += '<option value="weather">Weather</option>';
                        acc += '<option value="medical">Medical</option>';
                        acc += '<option value="covered">Covered</option>';
                        acc += '<option value="other" selected = "selected">Other</option>';
                        acc+= "</select>";
                        acc += '<td><button class="buttonedit other" data-toggle="modal" data-shiftId = "' + shift.imperfect_shift_number +'" data-target="#exampleModalCenter" data-reason = "' + shift.other + '" > <i class="far fa-file"></i></button></td>';

                    } else if(shift.reason === "Weather"){
                        acc += '<option value="na">N/A</option>';
                        acc += '<option value="weather" selected = "selected">Weather</option>';
                        acc += '<option value="medical">Medical</option>';
                        acc += '<option value="covered">Covered</option>';
                        acc += '<option value="other">Other</option>';
                        acc+= "</select>";
                        acc += '<td><button style = "display:none" class="buttonedit other" data-toggle="modal" data-shiftId = "' + shift.imperfect_shift_number +'" data-target="#exampleModalCenter" data-reason = "' + shift.other + '" > <i class="far fa-file"></i></button></td>';
                    } else if(shift.reason === "Medical"){
                        acc += '<option value="na">N/A</option>';
                        acc += '<option value="weather">Weather</option>';
                        acc += '<option value="medical" selected = "selected">Medical</option>';
                        acc += '<option value="covered">Covered</option>';
                        acc += '<option value="other">Other</option>';
                        acc+= "</select>";
                        acc += '<td><button style = "display:none" class="buttonedit other" data-toggle="modal" data-shiftId = "' + shift.imperfect_shift_number +'" data-target="#exampleModalCenter" data-reason = "' + shift.other + '" > <i class="far fa-file"></i></button></td>';
                    } else if(shift.reason === "Covered"){
                        acc += '<option value="na">N/A</option>';
                        acc += '<option value="weather">Weather</option>';
                        acc += '<option value="medical">Medical</option>';
                        acc += '<option value="covered" selected = "selected">Covered</option>';
                        acc += '<option value="other">Other</option>';
                        acc+= "</select>";
                        acc += '<td><button style = "display:none" class="buttonedit other" data-toggle="modal" data-shiftId = "' + shift.imperfect_shift_number +'" data-target="#exampleModalCenter" data-reason = "' + shift.other + '" > <i class="far fa-file"></i></button></td>';
                    } else{
                        acc += '<option value="na" selected = "selected">N/A</option>';
                        acc += '<option value="weather">Weather</option>';
                        acc += '<option value="medical">Medical</option>';
                        acc += '<option value="covered">Covered</option>';
                        acc += '<option value="other">Other</option>';
                        acc+= "</select>";
                        acc += '<td><button style = "display:none" class="buttonedit other" data-toggle="modal" data-shiftId = "' + shift.imperfect_shift_number +'" data-target="#exampleModalCenter" data-reason = "' + shift.other + '" > <i class="far fa-file"></i></button></td>';
                    }

                    if(shift.reason !== "Other")
                        acc += "<td></td>";

                    acc += "</tr>";
                }

                table.append(acc);
                $(".excused").change(updateExcused);
                $(".reason").change(updateReason);
                $(".buttonedit").click(function(event){
                        // Prevent Chrome Default Behavior of Submitting A From Whenever Any Button Is Clicked
                        event.preventDefault();
                        event.stopPropagation();
                        var element = $(event.target);
                        $("#modal-content").val(element.closest("button").data("reason"));
                        var btn = element.closest("button");
                        var id = btn.data("shiftid");
                        $("#shiftId").val(id);
                        $("#exampleModalCenter").modal('toggle');
                });
                loader.hide();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                loader.hide();
            });

        }

        function updateExcused(event){
            var element = $(event.target);
            var row = element.parent().parent();
            var data = {};
            data.imperfectShiftNumber = row.data("imperfectshiftid");
            if(element.find("option:selected").text() === "Excused"){
                data.excused = 1;
                row.find(".reason").prop("disabled", false);
            }else{
                data.excused = 0;
                data.reason = null;
                data.other = null;
                row.find(".reason").val("na");
                row.find(".reason").prop("disabled", true);
                row.find(".other").hide();
            }
            $.ajax({
                type:'GET',
                url:'<?=$config['webRoot']?>index.php?path=/api/imperfectShift/update',
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401)
                    return;
                alert("Failed to save changes to the database.");
            });
        }
        function updateReason(event){
            var element = $(event.target);
            var row = element.parent().parent();
            var data = {};
            data.imperfectShiftNumber = row.data("imperfectshiftid");
            data.reason = element.find("option:selected").text();
            if(element.find("option:selected").text() === "Other"){
                row.find(".other").show();
            }else{
                data.other = null;
                row.find(".other").hide();
            }
            $.ajax({
                type:'GET',
                url:'<?=$config['webRoot']?>index.php?path=/api/imperfectShift/update',
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401)
                    return;
                alert("Failed to save changes to the database.");
            });
        }

        function employeeFromName(name, employees) {
            for (var i = 0; i < employees.length; i++) {
                if (name === employees[i].firstName + ' ' + employees[i].lastName)
                    return employees[i];
            }
            return null;
        }


        $(document).ready(function() {
            inputEmployeeName = $("#forNameSearch");
            inputStartDate = $("#forStart");
            inputEndDate = $("#forEnd");
            loader = $("#spinner");
            loader.hide();
            $("#saveReason").click(function () {
                var other = $("#modal-content").val();
                var id = $("#shiftId").val();
                data = {
                    imperfectShiftNumber:id,
                    other:other
                };
                $.ajax({
                    type:'GET',
                    url:'<?=$config['webRoot']?>index.php?path=/api/imperfectShift/update',
                    dataType:'json',
                    statusCode: {
                        401: status401Handler
                    },
                    data: data
                }).done(function (data, textStatus, jqXHR) {
                    $("#exampleModalCenter").modal("toggle");
                    var selector = "[data-shiftid=" + $("#shiftId").val() + "]";
                    var btn = $("#performanceTable").find(selector);
                    btn.data("reason",  $("#modal-content").val());

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    baseAjaxErrorHandler(jqXHR);
                });
            });

            inputStartDate.datepicker();
            inputEndDate.datepicker();

            inputEmployeeName.change(searchFieldKeydown);
            inputStartDate.change(searchFieldKeydown);
            inputEndDate.change(searchFieldKeydown);

            table = $("#performanceTable").find("tbody");
            var employeeDatalist = $("#employees");
            employees = [];

            $.ajax({
                type:'GET',
                url:'<?=$config['webRoot']?>index.php?path=/api/employee',
                dataType:'json',
                statusCode: {
                    401: status401Handler
                }
            }).done(function (data, textStatus, jqXHR) {
                var acc = "";
                employees = data;
                for (var i = 0; i < data.length; i++) {
                    var name =  data[i].firstName + ' ' + data[i].lastName;
                    acc += '<option>' + name + '</option>';
                }
                employeeDatalist.html(acc);
            });

            $('#download').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                var url = '<?=$config['webRoot']?>index.php?path=/export/imperfectshift';
                var employee = employeeFromName(inputEmployeeName.val(), employees);
                if(employee != null) {
                    url += "&employeeId=" + employee.employeeId;
                }
                var startDate = moment($('#forStart').val(), 'MM/dd/YYYY').format('Y-MM-DD');
                if (startDate != 'Invalid date') {
                    url+= "&startDate=" + startDate;
                }
                var endDate = moment($('#forEnd').val(), 'MM/dd/YYYY').format('Y-MM-DD');
                if(endDate != 'Invalid date') {
                    url+= '&endDate=' + endDate;
                }
                //Provides a GET url for the function to run
                window.location.href = url;
            });
        });
    </script>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container">
    <h1>Employee Performance</h1>
    <form id="performanceRequest" novalidate>
        <div class="row">

            <div class="form-group col-lg-3 col-sm-12">
                <input list="employees" id="forNameSearch" type="text" autocomplete="off" class="form-control" placeholder="Type Name"/>
                <datalist id="employees"></datalist>
            </div>

            <div class="form-group col-lg-2 offset-lg-5 col-sm-12">
                <input type="text" autocomplete="off" placeholder="Start Date" class="form-control" id="forStart">
            </div>
            <div class="form-group col-lg-2 col-sm-12">
                <input type="text" autocomplete="off" placeholder="End Date" class="form-control" id="forEnd">
            </div>
        </div>
    </form>
    <div class="pre-scrollable form-group">
        <table class="table table-striped table-sm table-responsive-sm" id="performanceTable">
            <thead>
            <tr>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Date</th>
                <th scope="col">Start</th>
                <th scope="col">End</th>
                <th scope="col">Type</th>
                <th scope="col">Mins</th>
                <th scope="col">Excused</th>
                <th scope="col">Reason</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div id = "spinner" class="loader mx-auto"></div>
    </div>
    <div class="float-right">
        <button class="btn btn-lg btn-primary" id="download">Download</button>
    </div>

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Reason:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id = "modal-text">
                    <input id= "modal-content" class = "form-control" type = "text" maxlength="10000"/>
                    <input id = "shiftId" type="hidden" value = ""/>
                    <button id= "saveReason" class = "form-control" type = "submit">Save</button>
                </div>
            </div>
        </div>
    </div>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>
