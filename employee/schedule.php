<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Schedule</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>



<main role="main" class="container">
    <h1>Schedule</h1>

    <h3>Lab Workers</h3>
    <table class="table table-striped table-sm table-responsive-sm">
        <thead>
        <tr>
            <th>&nbsp</th>
            <th scope="col">Monday</th>
            <th scope="col">Tuesday</th>
            <th scope="col">Wednesday</th>
            <th scope="col">Thursday</th>
            <th scope="col">Friday</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $acc = "";
        for ($hour = 9; $hour < 17; $hour++) {
            if ($hour < 12)
                $hourStr = $hour . 'am';
            else if ($hour === 12)
                $hourStr = $hour . 'pm';
            else
                $hourStr = $hour - 12 . 'pm';

            $acc .= '<tr><th scope="row" class="text-nowrap">' .  $hourStr . '</th>';
            for ($day = 1; $day < 6; $day++) {
                $index = $day . ':' . $hour;
                $acc .=  '<td>';
                if (isset($labShiftHours[$index])) {
                    $leadingCharacter = '';
                    foreach ($labShiftHours[$index] as $shift) {
                        $acc .=  '<div style="display: inline">' . $leadingCharacter . $employeeMap[$shift->employeeId]->firstName . '</div>';
                        $leadingCharacter = '/';
                    }
                } else {
                    $acc .=  '&nbsp;';
                }
                $acc .=  '</td>';
            }
            $acc .=  '</tr>';
        }
        echo $acc;
        ?>
        </tbody>

    </table>

    <h3 style="margin-top: 75px">File Server Room Workers </h3>


    <table class="table table-striped table-sm table-responsive-sm">
        <thead>
        <tr>
            <th>&nbsp</th>
            <th scope="col">Monday</th>
            <th scope="col">Tuesday</th>
            <th scope="col">Wednesday</th>
            <th scope="col">Thursday</th>
            <th scope="col">Friday</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $acc = '';
        for ($hour = 9; $hour < 17; $hour++) {
            if ($hour < 12)
                $hourStr = $hour . 'am';
            else if ($hour === 12)
                $hourStr = $hour . 'pm';
            else
                $hourStr = $hour - 12 . 'pm';

            $acc .= '<tr><th scope="row" class="text-nowrap">' .  $hourStr . '</th>';
            for ($day = 1; $day < 6; $day++) {
                $index = $day . ':' . $hour;
                $acc .= '<td>';
                if (isset($fsrShiftHours[$index])) {
                    $leadingCharacter = '';
                    foreach ($fsrShiftHours[$index] as $shift) {
                        $acc .= '<div style="display: inline">' . $leadingCharacter . $employeeMap[$shift->employeeId]->firstName . '</div>';
                        $leadingCharacter = '/';
                    }
                } else {
                    $acc .= '&nbsp;';
                }
                $acc .= '</td>';
            }
            $acc .= '</tr>';
        }
        echo $acc;
        ?>
        </tbody>
    </table>
</main>
<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>