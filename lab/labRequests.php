<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Lab Reservation Request</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>
    <script>
        var lab_id = null;
        var repeating_lab_id = null;
        function submitAdd(data, done, fail) {
            $.ajax({
                type:'POST',
                url:"<?=$config['webRoot']?>index.php?path=/api/lab/create",
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
                //if there was a conflicts
                if(typeof data.conflicts !== 'undefined'){
                    var message = "Conflicts were found for the following entries:" + "<br>";
                    for(var i = 0; i < data.conflicts.length; i++){
                        message += data.conflicts[i] + "<br>";
                    }
                    $("#modal-text").html(message);
                    $("#exampleModalCenter").modal('toggle');
                } else {
                    window.location.reload(true);
                }

            }).fail(function (jqXHR, textStatus, errorThrown) {
                baseAjaxErrorHandler(jqXHR);
            })
        }

        function submitUpdate(data, done, fail) {
            $.ajax({
                type: 'POST',
                url: "<?=$config['webRoot']?>index.php?path=/api/lab/update",
                dataType: 'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
                done(data, textStatus, jqXHR);
                location.reload(true);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                fail(jqXHR, textStatus, errorThrown);
            })
        }

        $(document).ready(function() {
            //Date and time pickers
            $( "#datepicker" ).datepicker({
                minDate: 0
            });
            $( "#startDate").datepicker({
                minDate: 0
            });
            $( "#endDate").datepicker({
                minDate: 0
            });

            $("#exampleModalCenter").on('hidden.bs.modal', function(){
               location.reload(true);
            });


            //Stores JQuery selectors as variables
            var standardForm = $('#standardForm');
            var repeatForm = $('#repeatForm');
            var startTimeStandard = $("#startTime");
            var endTimeStandard = $("#endTime");
            var startTimeRepeating = $("#startTimeRepeat");
            var endTimeRepeating = $("#endTimeRepeat");
            var myReservations = $("#my-reservations");
            var myReservationsRepeating = $("#my-reservations-repeating");


            //Sets button functionality for attached button
            var singleLabRows = myReservations.find("tbody").find('tr');
            var repeatingLabRows = myReservationsRepeating.find("tbody").find('tr');

            for (var singleLabRow = 0; singleLabRow < singleLabRows.length; singleLabRow++) {
                var singleLabItem = $(singleLabRows[singleLabRow]);
                singleLabItem.find('td i').parent().click(function (e) {

                    var element = $(e.target).closest('td');
                    var data = {
                        lab_id: element.data("id")
                    };
                    $.ajax({
                        type: 'POST',
                        url: "<?=$config['webRoot']?>index.php?path=/api/lab/delete",
                        dataType: 'json',
                        statusCode: {
                            401: status401Handler
                        },
                        data: data
                    }).done(function (data, textStatus, jqXHR) {
                        element.closest('tr').remove();
                        $('#calendar').fullCalendar('refetchEvents');
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        baseAjaxErrorHandler(jqXHR);
                    });


                });
                singleLabItem.find('a').click(function (e) {
                    var element = $(e.target);
                    lab_id = element.parent().data("id");
                    repeating_lab_id = null;
                    var data = {
                        lab_id: lab_id
                    };
                    $.ajax({
                        type: 'POST',
                        url: "<?=$config['webRoot']?>index.php?path=/api/lab/fetch",
                        dataType: 'json',
                        statusCode: {
                            401: status401Handler
                        },
                        data: data
                    }).done(function(data, textStatus, jqXHR) {
                        repeatForm.hide();
                        standardForm.show();
                        var reservingClass = data.reserving_class.split(" ");
                        $('#forDept').val(data.dept);
                        $('#forClassNum').val(data.reserving_class);
                        $('#forClassSec').val(data.reserving_section);
                        $('#datepicker').val(data.date);
                        $('#startTime').val(data.start_time);
                        $('#endTime').val(data.end_time);
                        $('#repeatCheck').prop('checked', false);

                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        baseAjaxErrorHandler(jqXHR);
                    });
                })
            }
            //Sets button function for repeating reservations attached buttons
            for (var repeatingLabRow = 0; repeatingLabRow < repeatingLabRows.length; repeatingLabRow++) {
                var repeatingLabItem = $(repeatingLabRows[repeatingLabRow]);
                repeatingLabItem.find('td i').parent().click(function (e) {

                    var element = $(e.target).closest('td');
                    var data = {
                        repeating_reservation_id: element.data("id")
                    };
                    $.ajax({
                        type: 'POST',
                        url: "<?=$config['webRoot']?>index.php?path=/api/repeating_reservation/delete",
                        dataType: 'json',
                        statusCode: {
                            401: status401Handler
                        },
                        data: data
                    }).done(function (data, textStatus, jqXHR) {
                        location.reload(true);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        baseAjaxErrorHandler(jqXHR);
                    });


                });
                //Sets a link on the class for updating
                repeatingLabItem.find('a').click(function (e) {
                    var element = $(e.target);
                    lab_id = null;
                    repeating_lab_id = element.parent().data("id");
                    var data = {
                        repeating_reservation_id: repeating_lab_id
                    };
                    $.ajax({
                        type: 'POST',
                        url: "<?=$config['webRoot']?>index.php?path=/api/repeating_reservation/fetch",
                        dataType: 'json',
                        statusCode: {
                            401: status401Handler
                        },
                        data: data
                    }).done(function(data, textStatus, jqXHR) {
                        repeatForm.show();
                        standardForm.hide();
                        standardForm.children('input').prop('required', false);
                        repeatForm.children('input').prop('required', true);
                        var reservingClass = data.reserving_class.split(" ");
                        $('#forDept').val(reservingClass[0]);
                        $('#forClassNum').val(reservingClass[1]);
                        $('#forClassSec').val(reservingClass[2]);
                        $('#startDate').val(data.start_date);
                        $('#endDate').val(data.end_date);
                        $('#startTimeRepeat').val(data.start_time);
                        $('#endTimeRepeat').val(data.end_time);
                        var repeatCheck = $('#repeatCheck');
                        var repeat = repeatCheck.parent();
                        repeatCheck.prop('checked', true);
                        repeat.addClass('active', true);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        baseAjaxErrorHandler(jqXHR);
                    });
                })
            }

            //Requires the fields in regular reservations
            standardForm.children("input").prop('required', true);
            var today = moment();

            startTimeStandard.timepicker({
                'minTime': '9:00am',
                'maxTime': '5:00pm',
                'step': '15'
            });
            endTimeStandard.timepicker({
                'minTime': '9:00am',
                'maxTime': '5:00pm',
                'step': '15'
            });

            startTimeRepeating.timepicker({
                'minTime': '9:00am',
                'maxTime': '5:00pm',
                'step': '15'
            });
            endTimeRepeating.timepicker({
                'minTime': '9:00am',
                'maxTime': '5:00pm',
                'step': '15'
            });

            //Form
            var form = $("#labForm");

            form.on('submit', function(event) {
                // Prevent Chrome Default Behavior of Submitting A From Whenever Any Button Is Clicked
                event.preventDefault();
                event.stopPropagation();

                form.addClass('was-validated');

                //Stop Invalid Data From Submitting
                if (form[0].checkValidity() === false)
                    return;

                var reserveDate = moment($("#datepicker").val(), 'MM/DD/YYYY');
                var startTime = moment($("#startTime").val(), 'h:mmA');
                var endTime = moment($("#endTime").val(), 'h:mmA');
                var startTimeRepeat = moment($('#startTimeRepeat').val(), 'h:mmA');
                var endTimeRepeat = moment($('#endTimeRepeat').val(), 'h:mmA');
                var repeatCheck = $('input[name=repeatCheck]:checked').val();
                var dept = $("#forDept").val();
                var classSec = $("#forClassSec").val();
                if (classSec.length ===0) {
                    classSec = "C01";
                }


                var data = {
                    //Lab id for updating
                    lab_id: lab_id,
                    repeating_lab_id: repeating_lab_id,
                    //Required information for both types of reservations
                    dept: dept.toUpperCase(),
                    reserving_class: $("#forClassNum").val(),
                    lab: $('input[name=labType]:checked').val(),
                    reserving_section: classSec,
                    //Required information for single lab reservations
                    date: reserveDate.format('MM-DD-YYYY'),
                    startTime: startTime.format('HH:mm'),
                    endTime: endTime.format('HH:mm'),
                    //Required information for repeating reservations
                    repeatCheck: repeatCheck,
                    startDate: $('#startDate').val(),
                    endDate: $('#endDate').val(),
                    check_mon: $('#check_mon').is(':checked'),
                    check_tues: $('#check_tues').is(':checked'),
                    check_wed: $('#check_wed').is(':checked'),
                    check_thurs: $('#check_thurs').is(':checked'),
                    check_fri: $('#check_fri').is(':checked'),
                    startTimeRepeat: startTimeRepeat.format(('HH:mm')),
                    endTimeRepeat: endTimeRepeat.format('HH:mm')
                };

                var onSuccess = function (data, textStatus, jqXHR) {
                    //Refreshes calendar upon each event edition (if in the current date range)
                    $('#calendar').fullCalendar('refetchEvents');
                    location.reload(true);
                };

                var onFail = function (jqXHR, textStatus, errorThrown) {
                    baseAjaxErrorHandler(jqXHR);
                };

                var standardEqualTime = startTimeStandard.val() === endTimeStandard.val();
                var repeatEqualTime = startTimeRepeating.val() === endTimeRepeating.val();
                if(dept.toUpperCase() !== "CIS")
                {
                    if(reserveDate.diff(today, 'days') > 14){
                        alert("Please select a date within two weeks of today.");
                    }
                    else {
                        submitAdd(data, onSuccess, onFail);
                    }
                }
                else if (!repeatCheck) {
                    if (standardEqualTime) {
                        alert("Please select a different start and end time.");
                    }
                    else if (lab_id != null) {
                        submitUpdate(data, onSuccess, onFail);
                    }
                    else {
                        submitAdd(data, onSuccess, onFail);
                    }
                }
                else if (lab_id != null || repeating_lab_id != null) {
                    if (lab_id != null) {
                        if (standardEqualTime) {
                            alert("Please select a different start and end time");
                        }
                    }
                    if (repeating_lab_id != null) {
                        if (repeatEqualTime) {
                            alert("Please select a different start and end time");
                        }
                        else {
                            submitAdd(data, onSuccess, onFail);
                        }
                    }
                }
                else {
                    if (repeatEqualTime) {
                        alert("Please select different start and end time");
                    }
                    else {
                        submitAdd(data, onSuccess, onFail);
                    }
                }
            });


            // Init Full Calendar
            // Event Source (see: https://fullcalendar.io/docs/events-json-feed)
            $('#calendar').fullCalendar({
                themeSystem: 'bootstrap4',
                defaultView: 'agendaWeek',
                events: './index.php?path=/api/lab',
                weekends: false,
                allDaySlot: false,
                contentHeight: 'auto',
                minTime: '09:00:00',
                maxTime: '17:00:00'
                });

            myReservations.find('a').click(function (e) {
                e.preventDefault();
                $('#labForm')[0].scrollIntoView();
            });

            $("#repeatCheck").change(function () {
                if(this.checked) {
                    repeatForm.show();
                    standardForm.hide();

                    //Clears single reservation form
                    $('#datepicker').val("");
                    $('#startTime').val("");
                    $('#endTime').val("");

                    //Requires the fields in repeating reservations to be filled
                    repeatForm.children("input").prop('required', true);

                    //Clears requirements for single reservations
                    standardForm.children("input").prop('required', false);
                }
                else {
                    standardForm.show();
                    repeatForm.hide();

                    //Clears repeating reservation form
                    $('#startDate').val("");
                    $('#endDate').val("");
                    $('#startTimeRepeat').val("");
                    $('#endTimeRepeat').val("");
                    //Requires the fields in regular reservations
                    standardForm.children("input").prop('required', true);

                    //Unchecks repeating reservation checkboxes
                    var element = repeatForm.children('label');
                    repeatForm.children('label').removeClass('active');
                    var checkMon = $('#check_mon');
                    checkMon.prop('checked', false);
                    checkMon.parent().removeClass('active');

                    var checkTues = $('#check_tues');
                    checkTues.prop('checked', false);
                    checkTues.parent().removeClass('active');

                    var checkWed = $('#check_wed');
                    checkWed.prop('checked', false);
                    checkWed.parent().removeClass('active');

                    var checkThrus = $('#check_thrus');
                    checkThrus.prop('checked', false);
                    checkThrus.parent().removeClass('active');

                    var checkFri = $('#check_fri');
                    checkFri.prop('checked', false);
                    checkFri.parent().removeClass('active');

                    //Removes requirements from repeating reservation form
                    repeatForm.children("input").prop('required', false);
                }
            });
        })

    </script>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>
<main role="main" class="container">
    <h1>Lab Reservation Schedule</h1>
    <div class="row">
        <div style="margin-left: 25px; margin-right: 25px">
            <h4>Becker Lab Reservation Policy</h4>
            <h5>For Teaching Lab</h5>
            <ul>
                <li><b>CIS 217</b> can utilize the teaching lab during the class. Either the lab can be listed as  the classroom for the semester , or the lab be utilized as needed occasionally.</li>
                <li><b>Other CIS</b> courses can use the teaching lab in the FCFS (First Come First Served) basis only if there is no CIS 217 class. Reservations are not required, but are greatly appreciated by the lab management staff and will go a long way towards avoiding conflicts.</li>
                <li><b>Non-CIS</b> courses can reserve the teaching lab only if there is no CIS 217 class and there is no CIS course reserved in advance. A request can be sent no earlier than two weeks before the date.</li>
            </ul>

            <h5>For Outside Teaching Lab</h5>
            <ul>
                <li>Any course can utilize the computers outside the teaching lab in the FCFS (First Come First Served) basis. Reservations are not required, but are greatly appreciated.</li>
            </ul>
        </div>
        <div class="col-lg-9">
            <div id="calendar"></div>

        </div>
        <div class="col-lg-3 col-sm-7 container">
            <form id="labForm">
                <div class="row justify-content-center">
                    <h3 class="titleCentering">Request Form</h3>
                    <h5 class="titleCentering">Class</h5>
                    <div class="form-group row classForm">
                        <input id="forDept" type="text" autocomplete="off" class="form-control col" placeholder="ex. CIS" required>
                        <input id="forClassNum" type="text" autocomplete="off" class="form-control col" placeholder="ex. 217" required>
                        <input id="forClassSec" type="text" autocomplete="off" class="form-control col" placeholder="ex. C01">
                    </div>

                    <h5 class="titleCentering">Lab</h5>
                    <div class="form-group row">
                        <div class="btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-outline-info active">
                                <input type="radio"  name="labType" id="teaching" value = "teaching" autocomplete="off" checked> Teaching
                            </label>
                            <label class="btn btn-outline-success">
                                <input type="radio"  name="labType" id="main" value = "outside" autocomplete="off"> Outside
                            </label>
                        </div>
                    </div>

                    <h5 class="titleCentering">Date and Time</h5>

                    <div class="btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-outline-secondary">
                            <input type="checkbox" id="repeatCheck" name='repeatCheck' autocomplete="off" > Repeat
                        </label>
                    </div>

                    <div class="form-group row container" id="standardForm">
                        <label for="datepicker">Date</label>
                        <input type="text" name = "date" autocomplete="off" class="form-control" id="datepicker">


                        <label for="startTime">Start</label>
                        <input id="startTime" type="text" autocomplete="off" name = "startTime" class=" form-control time ui-timepicker-input" >

                        <label for="endTime">End</label>
                        <input id="endTime" type="text"  autocomplete="off" name = "endTime" class=" form-control time ui-timepicker-input" >
                    </div>
                    <div class="form-group row container" id="repeatForm" style="display: none">
                        <label for="startDate">Start Date</label>
                        <input id="startDate" autocomplete="off" class="form-control" type="text">

                        <label for="endDate">End Date</label>
                        <input id="endDate" autocomplete="off" class="form-control" type="text">

                        <label for="startTimeRepeat">Start Time</label>
                        <input id="startTimeRepeat" type="text" autocomplete="off" class=" form-control time ui-timepicker-input" >

                        <label for="endTimeRepeat">End Time</label>
                        <input id="endTimeRepeat" type="text" autocomplete="off" class=" form-control time ui-timepicker-input">

                        <div class="btn-group-toggle titleCentering" data-toggle="buttons">
                            <label class="btn btn-outline-secondary btn-sm">
                                <input type="checkbox" id ="check_mon" autocomplete="off"> M
                            </label>
                            <label class="btn btn-outline-secondary btn-sm">
                                <input type="checkbox" id = "check_tues" autocomplete="off"> T
                            </label>
                            <label class="btn btn-outline-secondary btn-sm">
                                <input type="checkbox" id = "check_wed" autocomplete="off"> W
                            </label>
                            <label class="btn btn-outline-secondary btn-sm">
                                <input type="checkbox" id = "check_thurs" autocomplete="off"> T
                            </label>
                            <label class="btn btn-outline-secondary btn-sm">
                                <input type="checkbox" id = "check_fri" autocomplete="off"> F
                            </label>

                        </div>
                    </div>

                    <button class="btn btn-lg btn-primary row" type="submit">Submit Request</button>
                </div>

            </form>
        </div>
        <div class="col-12">

            <h3>Schedule</h3>

            <h4>Individual Reservations</h4>
            <table id="my-reservations" class="table table-striped table-sm table-responsive-sm">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Class</th>
                    <th scope="col">Lab</th>
                    <th scope="col">Start</th>
                    <th scope="col">End</th>
                    <th scope="col">Date</th>
                    <th scope="col">&nbsp;</th>
                </tr>
                </thead>
                <?php
                foreach ($individualReservations as $individualReservation) {?>
                <tr>
                    <td> <?=$individualReservation->faculty_username ?> </td>
                    <td data-id = "<?= $individualReservation->reservation_id?>">
                        <a href = "#standardForm">
                            <?=$individualReservation->dept . " " . $individualReservation->reserving_class ." " . $individualReservation->reserving_section ?>
                        </a>
                    </td>
                    <td> <?=($individualReservation->is_teaching_lab == 1) ? "Teaching":"Outside" ?> </td>
                    <td> <?=$individualReservation->start_time->format("g:ia") ?> </td>
                    <td> <?=$individualReservation->end_time->format("g:ia") ?> </td>
                    <td> <?=$individualReservation->date ?> </td>
                    <td data-id="<?= $individualReservation->reservation_id?>"><i class = "fas fa-trash"></i></td>
                </tr>
                <?php } ?>

            </table>

            <h4>Recurring Reservations</h4>
            <table id="my-reservations-repeating" class="table table-striped table-sm table-responsive-sm">
                <thead>

                <tr>
                    <th scope="col"></th>
                    <th scope="col">Class</th>
                    <th scope="col">Lab</th>
                    <th scope="col">Start</th>
                    <th scope="col">End</th>
                    <th scope="col">Start</th>
                    <th scope="col">End</th>
                    <th scope="col">Repeats</th>
                    <th scope="col">&nbsp;</th>
                </tr>
                </thead>
                <?php
                    foreach ($repeatingReservations as $repeatingReservation) {?>
                        <tr>
                            <td> <?=$repeatingReservation->reserving_faculty_username ?> </td>
                            <td data-id="<?= $repeatingReservation->repeating_res_id ?>">
                                <a href = "#repeatForm"> <?=$repeatingReservation->reserving_class ?> </a>
                            </td>
                            <td> <?=($repeatingReservation->is_teaching_lab == 1) ? "Teaching":"Outside" ?> </td>
                            <td> <?=$repeatingReservation->start_time->format("g:ia") ?> </td>
                            <td> <?=$repeatingReservation->end_time->format("g:ia") ?> </td>
                            <td> <?=$repeatingReservation->start_date ?> </td>
                            <td> <?=$repeatingReservation->end_date ?> </td>
                            <td> <?= $repeatingReservation->days_string ?></td>
                            <td data-id="<?= $repeatingReservation->repeating_res_id?>"><i class = "fas fa-trash"></i></td>
                        </tr>
                <?php } ?>

            </table>
        </div>
    </div>
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Conflicts Found</h5>
                    <button type="button" id="closeBtn" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id = "modal-text">

                </div>
            </div>
        </div>
    </div>
</main>
<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>
