<?php
date_default_timezone_set("America/New_York");
require_once 'persistence/mysql.php';
require_once 'persistence/timeclockApi.php';
require_once 'email.php';
$config = require 'config.php';
const AUTH_COOKIE_NAME = 'beckerlabAuth';

function isLoggedIn() {
    return isset($_SESSION['userId']);
}

function isFaculty() {
    if (!isLoggedIn())
        return false;
    $user = getUserById($_SESSION['userId']);
    if (is_null($user))
        return false;
    return $user->role === 'faculty';
}

function isFsr() {
    if (!isLoggedIn())
        return false;
    $user = getUserById($_SESSION['userId']);
    if (is_null($user))
        return false;
    return $user->role === 'fsr';
}

function getCurrentUserEmail(): string {
    if (!isLoggedIn())
        return '';
    $user = getUserById($_SESSION['userId']);
    if (is_null($user))
        return '';

    return $user->email;
}

/**
 * Compares Two DateTime Objects By Their Unix Timestamp
 *
 * @param DateTime $lhs
 * @param DateTime $rhs
 * @return bool
 */
function datetimeEqual(DateTime $lhs, DateTime $rhs): bool {
    return intval($lhs->format('U')) == intval($rhs->format('U'));
}

/**
 * Authenticates a Clarion account against the campus authentication servers.
 * This function depends on Kerberos being correctly configured on the
 * machine that hosts the web server as well as the kinit command
 * needs to be available to the Apache user.
 *
 * @author Evan Black
 *
 * @param string $username
 * The username for the account we are authenticating. With an optional realm (the @clarion.local part).
 * If the realm is specified, then it must match the realm configuration on the server!
 * (i.e. Just because normally a user signs in like s_epblack@clarion.edu does not mean the server has
 * the Clarion realm named clarion.edu)
 *
 * @param string $password
 * The password for the user we are authenticating.
 * Be careful with passwords that contain things like single quotes (') or newline characters (\n)
 * as the shell may try to interpret them.
 *
 * @see https://pwss.clarion.edu/changePassword.asp The Computing Services Change Password Page (Allowed Password Characters)
 * @see https://linux.die.net/man/1/kinit The Manual Page For kinit
 *
 * @return bool
 * true: If kinit returned success (0, no error), false otherwise
 */
function clarionLogin(string $username, string $password): bool {
    // Create A Temporary File Read/Write -able only by us (permission: 0600)
    // And Store The Password In That File
    // We Do This So Our Password Does Not Appear In The Process List While Our Shell Command Is Running
    $tmpFile = tempnam(sys_get_temp_dir(), 'usr');
    file_put_contents($tmpFile, $password);

    // Kerberos is how we interact with the Clarion Authentication Servers
    // Here we:
    //  Read the password out of a temporary file (To prevent it from being in the output of ps)
    //  Pipe the Password to the `kinit` Function (Since It Would Normally ask us for one if we just ran it)
    //  Call the `kinit` command with the username as an argument (Do Not Pass The @clarion.edu Part)
    //      FYI: As of this witting, the 'domain' (a Realm in Kerberos) is @CLARION.LOCAL, try not to depend on this value
    //  We Redirect all normal (non-error) output of the `kinit` command to /dev/null, effectively ignoring it
    //  If the command succeeded (kinit returned 0, implies username/pass are correct), then the `printf` on the right side of the and (`&&`) is run
    //  If the command fails, then PHP's shell_exec is defined to return null (See: http://php.net/manual/en/function.shell-exec.php)
    //  Both the username and the password are wrapped in `escapeshellarg` calls, this function is
    //      Supposed to protect from shell injection, though it may still be possible.
    //      Try to depend on them as little as possible
    //
    // Returns "Success" If username/password are correct
    // null Otherwise
    $shellResult = shell_exec('cat ' .  $tmpFile . " | kinit " . escapeshellarg($username) . " > /dev/null && printf 'Success\n'");

    // Remove Our Temporary File As Soon As It's Unneeded
    unlink($tmpFile);
    return trim($shellResult) === "Success";
}

/**
 * Creates a map of employees from the Timeclock API Response
 *
 * @param \timeclock\Response $employeeResponse
 * The Direct Response from the TimeClock API
 * If the data field is not set an empty array is returned
 *
 * @return array
 * An Associative Array in the form [employeeId => Employee]
 */
function mapEmployees(\timeclock\Response $employeeResponse) {
    if (isset($employeeResponse->data)) {
        $employees = $employeeResponse->data;
        $employeeMap = [];
        foreach ($employees as $employee) {
            $employeeMap[$employee->employeeId] = $employee;
        }
        return $employeeMap;
    }
    return [];
}

/**
 * @param int [$employeeId]
 * @param DateTime [$startDate]
 * @param DateTime [$endDate]
 * @return array
 *
 * @throws Exception
 * When The Time Clock Persistence Fails To Retrieve Employees
 */
function getImperfectShifts(int $employeeId = 0, DateTime $startDate = null, DateTime $endDate = null): array {
    $employeeResp = timeclock\getEmployees();

    if (isset($employeeResp->data)) {
        $employeeMap = mapEmployees($employeeResp);
    } else {
        throw new Exception("Failed To Retrieve Employees");
    }


    if ($employeeId > 0 && isset($startDate)) {
        if (isset($endDate)) {
            $imperfectShifts = getImperfectShiftByEmployeeAndDate($employeeId, $startDate, $endDate);
        } else {
            $imperfectShifts = getImperfectShiftByEmployeeAndDate($employeeId, $startDate);
        }
    } elseif ($employeeId > 0) {
        $imperfectShifts = getImperfectShiftsByEmployee($employeeId);
    } elseif (isset($startDate)) {
        if (isset($endDate)) {
            $imperfectShifts = getImperfectShiftsByDate($startDate, $endDate);
        } else {
            $imperfectShifts = getImperfectShiftsByDate($startDate);
        }
    } else {
        $imperfectShifts = [];
    }

    return array_map(function ($imperfectShift) use (&$employeeMap) {
        $employeeId = $imperfectShift->employeeId;
        if (isset($employeeMap[$employeeId])) {
            $imperfectShift->firstName = $employeeMap[$employeeId]->firstName;
            $imperfectShift->lastName = $employeeMap[$employeeId]->lastName;
        }
        return $imperfectShift;
    }, $imperfectShifts);
}


// Enforce STS If We Want HTTPS
if ($config['forceHttps']) {
    header('Strict-Transport-Security: max-age=31536000');
}

session_start();

// Check If We're Logged Out, But Passed An Auth Cookie
if (!isLoggedIn() && isset($_COOKIE[AUTH_COOKIE_NAME])) {
    $splitCookie = explode(':', $_COOKIE[AUTH_COOKIE_NAME], 2);
    if (count($splitCookie) === 2) {
        deleteOldAuthCookies();
        $cookieId = intval($splitCookie[0]);

        $cookie = getAuthCookieById($cookieId, AuthCookie::hashAuthenticator($splitCookie[1]));

        // getAuthCookieByUserIdAndAuthenticator Performs Hash Checking
        if ($cookie !== null) {
            $_SESSION['userId'] = $cookie->userId;
            // Extend The Auth Cookie
            $expires = time() + $config['authCookieExpires'];
            $cookie->expires = DateTime::createFromFormat('U', $expires);
            persistAuthCookie($cookie);
            // Update The User's Cookie
            setcookie(AUTH_COOKIE_NAME,
                $_COOKIE[AUTH_COOKIE_NAME],
                $expires,
                $config['webRoot'],
                $_SERVER["SERVER_NAME"],
                false,
                true);
        } else {
            // Expire Invalid Cookie
            setcookie(AUTH_COOKIE_NAME, '', 0);
        }
    } else {
        // Expire Invalid Cookie
        setcookie(AUTH_COOKIE_NAME, '', 0);
    }
}


if (isset($_GET['path']))
    $path = $_GET['path'];
else if (isset($_POST['path']))
    $path = $_POST['path'];
else
    $path = '/home';

switch ($path) {
    /**
     * Shows Home Page
     */
    case '/home':
        require_once 'home.php';
        break;

    /**
     * Shows Contact Page
     */
    case '/contact':
        require_once 'contact.php';
        break;
    /**
     * Shows Login Page
     */
    case '/login':
        require_once 'login.php';
        break;
    /**
     * API Call For Logging In A User
     *
     * @method: POST
     *
     * @parameter username {String}
     *  Email/Username of The User To Login
     *
     * @parameter password {String}
     *  Password of The User To Login
     *
     * @return
     *  400: No Email/Password, Invalid Email/Password, User Not Found
     *  200: User Found & Session Started
     *      json: { userId:(the User's ID) } + Auth Cookie
     */
    case '/api/login':
        if (!isset($_POST['username'])) {
            header(' ', true, 400);
            $resp['message'] = 'No Username Provided';
            echo json_encode($resp);
            die();
        }
        if (!isset($_POST['password'])) {
            header(' ', true, 400);
            $resp['message'] = 'No Password Provided';
            echo json_encode($resp);
            die();
        }

        // Separate The Username Portion From clarion.edu Or localhost
        $splitUsername = explode('@', trim($_POST['username']));

        $username = $splitUsername[0];
        $plainText = $_POST['password'];

        $host = "clarion.edu";
        if (isset($splitUsername[1]))
            $host = $splitUsername[1];

        $user = getUserByUsername($username);

        if (is_null($user)) {

            // Prevent Student Accounts From Generating Faculty Accounts
            if (substr($username, 0, 2) === 's_') {
                header(' ', true, 403);
                echo json_encode([ 'message' => 'Students May Not Login Unless Expressly Allowed' ]);
                die();
            }

            // If The User Is A Clarion User
            // And Username/Password Are Correct,
            // But We Don't Have A Record Of Them
            // Add One
            if (strtolower($host) === "clarion.edu" && clarionLogin($username, $plainText)) {
                // Clarion Emails Match Username @clarion.edu [For Faculty, Not Students]
                $email = $splitUsername[0] . '@clarion.edu';
                $userId = createClarionUser($username, $email, 'faculty', true);

                if ($userId > 0) {
                    /** @noinspection PhpUnhandledExceptionInspection */
                    $authValue = random_bytes(64);
                    $expires = time() + $config['authCookieExpires'];
                    $cookieId = persistAuthCookie(AuthCookie::create($userId, $authValue, $expires));
                    $cookieValue =  $cookieId . ':' . $authValue;
                    setcookie(AUTH_COOKIE_NAME,
                        $cookieValue,
                        $expires,
                        $config['webRoot'],
                        $_SERVER["SERVER_NAME"],
                        false,
                        true);
                    $_SESSION['userId'] = $userId;
                    echo json_encode([ 'message' => 'success' ]);
                    die();
                } else {
                    header(' ', true, 500);
                    echo json_encode([ 'message' => 'Failed to create new Clarion User' ]);
                    die();
                }
            } else { // User Not Found & Not A New Clarion User
                header(' ', true, 400);
                echo json_encode(['message' => 'Email/Password Not Valid']);
                die();
            }
        } else { // User Found. Verify Password Based On Host

            $isValidLogin = $host === "clarion.edu" && clarionLogin($username, $plainText);
            if (!$isValidLogin)
                $isValidLogin = $host === "localhost" && verifyPassword($plainText, $user->salt, $user->hashedPass);


            if ($isValidLogin) {

                if (!$user->isActive) {
                    header(' ', true, 403);
                    echo json_encode([ 'message' => 'User Has Been Disabled, contact beckerlab@clarion.edu if you believe this is in error' ]);
                    die();
                }

                /** @noinspection PhpUnhandledExceptionInspection */
                $authValue = random_bytes(64);
                $expires = time() + $config['authCookieExpires'];
                $cookieId = persistAuthCookie(AuthCookie::create($user->id, $authValue, $expires));
                $cookieValue =  $cookieId . ':' . $authValue;
                setcookie(AUTH_COOKIE_NAME,
                    $cookieValue,
                    $expires,
                    $config['webRoot'],
                    $_SERVER["SERVER_NAME"],
                    false,
                    true);
                $_SESSION['userId'] = $user->id;
                echo json_encode(['message' => 'success']);
                die();
            } else {
                header(' ', true, 400);
                echo json_encode(['message' => 'Email/Password Not Valid']);
            }
        }
        break;
    /**
     * Ends The Current Session And Dumps The User At The Homepage
     * Expires/Removes The Auth Cookie
     */
    case '/logout':
        // Clear Only The Cookie For This Browser
        if (isset($_COOKIE[AUTH_COOKIE_NAME])) {
            $cookieValue = explode(':', $_COOKIE[AUTH_COOKIE_NAME], 2);
            if (count($cookieValue) === 2) {
                $cookieId = intval($cookieValue[0]);
                deleteAuthCookieById($cookieId);
            }

            // Set The Cookie To Expire A Long Time Ago
            // Effectively 'Deleting' It
            setcookie(AUTH_COOKIE_NAME, '', 0);
        }

        $_SESSION = array();
        session_destroy();
        require_once 'home.php';
        break;
    /**
     * API Method For Retrieving Requests
     * If Called From A Faculty Account, Only Retrieves Their Drives
     * If Called From A FSR Account, Retrieves All Drives
     *
     * @method GET
     *
     * @parameter [id] {int}
     *  ID of A Request To Retrieve
     *
     * @parameter [status] {string}
     *  Type of Status (Open/Closed) To Retrieve Requests From
     *
     * @return
     *  200:
     *      If id Is Not Set, Then An Array of Requests
     *      If status Is Set, Then Only Requests of That Status
     *      If Called From A Faculty Account, Only Retrieves Their Drives
     *      If Called From A FSR Account, Retrieves All Drives
     *  401:
     *      User Is Not Logged In
     *
     */
    case '/api/requests':
        if (!isset($_SESSION['userId'])) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            echo json_encode($resp);
            die();
        }

        if (isset($_GET['id'])) {
            echo json_encode(getRequestById($_GET['id']));
            die();
        }

        if (isFsr()) {
            if (isset($_GET['status']))
                echo json_encode(getRequestsByStatus($_GET['status']));
            else
                echo json_encode(getAllRequests());
            die();
        }

        if (isset($_GET['status'])) {
            $requests = getRequestsForUserByStatus($_SESSION['userId'], $_GET['status']);
        } else {
            $requests = getRequestsForUser($_SESSION['userId']);
        }

        // Encode Dates As Strings For Browser
        array_walk($requests, function (Request &$item) {
            if (isset($item->created)) {
                $item->created = $item->created->format('m/d/Y H:i');
            }
        });
        echo json_encode($requests);
        die();
        break;
    /**
     * API Method For Updating A Drive Request
     * A Drive Request May Only Be Modified By The Creator, Or By An FSR Member
     * Only Provided Fields Will Be Modified
     * ID/Creator/Created Cannot Be Modified
     *
     * @method POST
     *
     * @parameter id {int}
     *  ID of The Request To Modify
     *
     * @parameter [driveClass] {string}
     *  Class That Is Requesting The Drives (ex: CIS 402)
     *
     * @parameter [drives] {int}
     *  Number of Drives Requested, Should Be Greater Than 0
     *
     * @parameter [operatingSystem] {string}
     *  The Operating System To Be Installed On The Drives
     *
     * @parameter [description] {string}
     *  Additional Software / Description of Requirements For The Drives
     *
     * @parameter [status] {string} (Open|Closed)
     *  Status of The Request, Open Means The Request Has Not Yet Been Fulfilled
     *  Closed Means The Drive Request Has Been Fulfilled
     *
     * @return
     *  200:
     *      The Request Was Successfully Modified
     *      json: { id: (the ID of The Modified Request) }
     *  401:
     *      The User Is Not Logged In
     *  403:
     *      The User Is Not Authorized To Modify This Request
     *  404:
     *      The Request Was Not Found, or No id Was Provided
     */
    case '/api/requests/update':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            echo json_encode($resp);
            die();
        }

        if (!isset($_POST['id'])) {
            header(' ', true, 404);
            $resp['message'] = 'Not Found';
            echo json_encode($resp);
            die();
        }

        $request = getRequestById($_POST['id']);
        if ($request == null) {
            header(' ', true, 404);
            $resp['message'] = 'Not Found';
            echo json_encode($resp);
            die();
        }

        if (!isFsr() && $request->userId !== $_SESSION['userId']) {
            header(' ', true, 403);
            $resp['message'] = 'Unauthorized';
            echo json_encode($resp);
            die();
        }

        if (isset($_POST['driveClass']))
            $request->class = $_POST['driveClass'];

        if (isset($_POST['drives']))
            $request->drives = $_POST['drives'];

        if (isset($_POST['operatingSystem']))
            $request->operatingSystem = $_POST['operatingSystem'];

        if (isset($_POST['description']))
            $request->other = $_POST['description'];

        if (isset($_POST['status']))
            $request->status = $_POST['status'];

        $response['id'] = persistRequest($request);

        if ($config['enableEmailNotifications']) {
            try {
                email\driveRequestUpdated(getCurrentUserEmail(), $request, $config);
            } catch (\email\EmailException $e) {
            }
        }

        echo json_encode($response);
        die();
        break;
    /**
     * API Method For Creating A Drive Request
     * A Drive Request May Be Created By an FSR or Faculty Account
     * Drives Are Always Created With Open Status
     *
     * @method POST
     *
     * @parameter driveClass {string}
     *  Class That Is Requesting The Drives (ex: CIS 402)
     *
     * @parameter drives {int}
     *  Number of Drives Requested, Should Be Greater Than 0
     *
     * @parameter operatingSystem {string}
     *  The Operating System To Be Installed On The Drives
     *
     * @parameter description {string}
     *  Additional Software / Description of Requirements For The Drives
     *
     * @return
     *  201:
     *      The Request Was Successfully Created
     *      json: { id: (the ID of The New Request) }
     *  401:
     *      The User Is Not Logged In
     */
    case '/api/requests/create':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            echo json_encode($resp);
            die();
        }

        $request = new \Request();
        $request->userId = $_SESSION['userId'];
        $request->class = $_POST['driveClass'];
        $request->drives = $_POST['drives'];
        $request->operatingSystem = $_POST['operatingSystem'];
        $request->other = $_POST['description'];
        $request->status = 'Open';

        $response['id'] = persistRequest($request);

        if ($config['enableEmailNotifications']) {
            try {
                email\driveRequestCreated(getCurrentUserEmail(), $request, $config);
            } catch (\email\EmailException $e) {
            }
        }

        header(' ', true, 201);
        echo json_encode($response);
        break;

    /**
     * API Method For Deleting A Drive Request
     * A Drive Request May Only Be Deleted By The Creator, Or By An FSR Member
     *
     * @method POST
     *
     * @parameter id {int}
     *  ID of The Request To Delete
     *
     * @return
     *  200:
     *      The Request Was Successfully Deleted
     *      json: { id: (the ID of The Deleted Request) }
     *  401:
     *      The User Is Not Logged In
     *  403:
     *      The User Is Not Authorized To Delete This Request
     */
    case '/api/requests/delete':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = 'Unauthorized';
            echo json_encode($resp);
            die();
        }
        $request = getRequestById($_POST['id']);
        if ($request == null || $request->id == null) {
            header(' ', true, 404);
            die();
        }

        if (!isFsr() && $request->userId !== $_SESSION['userId']) {
            header(' ', true, 403);
            $resp['message'] = 'Unauthorized';
            echo json_encode($resp);
            die();
        }

        deleteRequest($request->id);
        $resp['id'] = $_POST['id'];
        echo json_encode($resp);
        break;

    case '/api/schedule/create':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            echo json_encode([ 'message' => 'Not Logged In' ]);
            die();
        }
        if (!isFsr()) {
            header(' ', true, 403);
            echo json_encode([ 'message' => 'Unauthorized' ]);
            die();
        }
        if (!isset($_POST['start'])) {
            header(' ', true, 400);
            echo json_encode([ 'message' => 'Start Date Is Required' ]);
            die();
        }
        $startDate = DateTime::createFromFormat('m/d/Y', $_POST['start']);
        if ($startDate === false) {
            header(' ', true, 400);
            echo json_encode([ 'message' => 'Start Date Is Invalid' ]);
            die();
        }
        $startDate->setTime(0,0);
        $now = new DateTime('midnight');
        if ($startDate < $now) {
            header(' ', true, 400);
            echo json_encode([ 'message' => 'Start Date Must Not Be Before Today' ]);
            die();
        }

        if (!isset($_POST['end'])) {
            header(' ', true, 400);
            echo json_encode([ 'message' => 'End Date Is Required' ]);
            die();
        }
        $endDate = DateTime::createFromFormat('m/d/Y', $_POST['end']);
        if ($endDate === false) {
            header(' ', true, 400);
            echo json_encode([ 'message' => 'End Date Is Invalid' ]);
            die();
        }
        $endDate->setTime(0,0);
        if ($endDate <= $startDate) {
            header(' ', true, 400);
            echo json_encode([ 'message' => 'End Date Must Be After Start Date' ]);
            die();
        }

        $oneWeeksFromNow = clone $now;
        /** @noinspection PhpUnhandledExceptionInspection */
        $oneWeeksFromNow->add(new DateInterval('P1W'));

        if ($startDate > $oneWeeksFromNow) {
            header(' ', true, 400);
            echo json_encode([ 'message' => 'Start Date May Not Be More Than One Week In The Future' ]);
            die();
        }

        $dayBeforeStartDate = clone $startDate;
        /** @noinspection PhpUnhandledExceptionInspection */
        $dayBeforeStartDate->sub(new DateInterval('P1D'));

        // Detect If An Old Schedule Is Active In
        // The New Schedule's Date Range
        $oldSchedules = getSchedulesByDates($startDate, $endDate);
        foreach ($oldSchedules as $oldSchedule) {
            if ($oldSchedule->endActiveDate >= $startDate) {
                $oneDayAgo = clone $oldSchedule;
                $oneDayAgo->endActiveDate = clone $dayBeforeStartDate;

                $diff = $oneDayAgo->startActiveDate->diff($oneDayAgo->endActiveDate, true);

                // If The Old Schedule's New End Date Makes The
                // Duration Of The Schedule Less Then A Day
                // (Making The Schedule Have No Effect)
                // Delete It, Otherwise, Update With The
                // New End Date
                if ($diff->y > 1 || $diff->m > 1 || $diff->d > 1) {
                    updateSchedule($oneDayAgo);
                } else {
                    deleteSchedule($oldSchedule);
                }

            }
        }

        $schedule = new Schedule();
        $schedule->startActiveDate = $startDate;
        $schedule->endActiveDate = $endDate;
        $scheduleId = insertSchedule($schedule);
        if ($scheduleId < 1) {
            header(' ', true, 500);
            echo json_encode([ 'message' => 'Failed To Insert Schedule' ]);
            die();
        }


        $shifts = [];
        foreach ($_POST['schedule'] as $employeeId => $scheduleEmployee) {
            foreach ($scheduleEmployee as $dayOfWeek => $scheduleEmployeeDay) {
                $lastPushedShiftIndex = -1;

                foreach ($scheduleEmployeeDay as $shiftIndex => $scheduleEmployeeDayShift) {
                    $shift = new EmployeeShift();
                    $shift->dayOfShift = $dayOfWeek;
                    $shift->employeeId = $employeeId;
                    $shift->scheduleNumber = $scheduleId;

                    $shiftStart = intval($scheduleEmployeeDayShift['time']) - 1 . ":55";
                    $shift->shiftStart = DateTime::createFromFormat('H:i', $shiftStart);

                    $shiftEnd =  DateTime::createFromFormat('H:i', intval($scheduleEmployeeDayShift['time']) . ":55");

                    // Save The Shift For Display Before Altering The End Time
                    $rawShift = RawEmployeeShift::createFromShift($shift, $scheduleEmployeeDayShift['type']);
                    $rawShift->shiftEnd = clone $shiftEnd;
                    insertRawEmployeeShift($rawShift);

                    // If We've already inserted a shift for today &
                    // The last pushed shift was from the same employee &
                    // The end time of the last shift is our start time
                    //
                    // Then the shifts are combined (by moving the old end date)
                    // Otherwise, they are separated by at least one hour
                    if ($lastPushedShiftIndex > -1 &&
                        $shifts[$lastPushedShiftIndex]->employeeId == $shift->employeeId &&
                        datetimeEqual($shift->shiftStart, $shifts[$lastPushedShiftIndex]->shiftEnd)) {
                        $shifts[$lastPushedShiftIndex]->shiftEnd = clone $shiftEnd;
                    } else {
                        $shift->shiftEnd = clone $shiftEnd;
                        $lastPushedShiftIndex = array_push($shifts, $shift) - 1; // -1 Since Array Push Returns New Length, Not Index
                    }
                }
            }
        }


        $ids = insertManyEmployeeShift($shifts);
        if (count($ids)) {
            header(' ', true, 201);
            echo json_encode([ 'ids' => $ids ]);
        } else {
            header(' ', true, 204);
        }
        die();
        break;

    case '/api/employee':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            echo json_encode($resp);
            die();
        }

        $department = (isset($_GET['department'])) ? $_GET['department'] : 'CIS DEPT';
        $activeOn = (isset($_GET['activeOn'])) ? $_GET['activeOn'] : '10-19-1995';

        $results = timeclock\getEmployees($department, $activeOn);

        // If We Got Results, Pass Them Back
        if (!is_null($results->data)) {
            echo json_encode($results->data);
            die();
        }

        // Pass Back Error
        header(' ', true, 400);
        echo json_encode($results);
        break;

    case '/api/imperfectshift':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            echo json_encode($resp);
            die();
        } else if (!isFsr()) {
            header(' ', true, 403);
            $resp['message'] = "Not Authorised";
            json_encode($resp);
            die();
        }

        $startDate = null;
        if (isset($_GET['startDate'])) {
            $startDate = date_create_from_format('Y-m-d', $_GET['startDate']);
            if ($startDate === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }
        }

        $endDate = null;
        if (isset($_GET['endDate'])) {
            $endDate = date_create_from_format('Y-m-d', $_GET['endDate']);
            if ($endDate === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }
        }

        if (isset($_GET['employeeId'])) {
            $employeeId = intval($_GET['employeeId']);
            if (!is_int($employeeId)) {
                header(' ', true, 400);
                $resp['message'] = "Employee ID Not Valid";
                echo json_encode($resp);
                die();
            }
        } else {
            $employeeId = 0;
        }

        try {
            $imperfectShifts = getImperfectShiftInfo($employeeId, $startDate, $endDate);
            $employees = \timeclock\getEmployees()->data;
            foreach($imperfectShifts as $imperfectShift) {
                foreach($employees as $employee){
                    if(intval($imperfectShift->employeeId) == $employee->employeeId){
                        $imperfectShift->firstName = $employee->firstName;
                        $imperfectShift->lastName =$employee->lastName;
                    }
                }
            }
            echo json_encode($imperfectShifts);
        } catch (Exception $e) {
            echo json_encode([ "Error" => "Failed To Retrieve Employees"]);
            header(' ', true, 500);
        }
        die();
        break;
    case '/api/imperfectShift/update':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            echo json_encode($resp);
            die();
        } else if (!isFsr()) {
            header(' ', true, 403);
            $resp['message'] = "Not Authorised";
            json_encode($resp);
            die();
        }

        if(!isset($_GET['imperfectShiftNumber'])){
            header(' '. true, 400);
            $resp['message'] = "No imperfect shift id given.";
            json_encode($resp);
            die();
        }

        $imperfectShift = new \ImperfectShift();
        $imperfectShift = getImperfectShift($_GET['imperfectShiftNumber']);

        if(isset($_GET['excused'])) {
            $imperfectShift->excused = $_GET['excused'];
        }
        if(isset($_GET['reason'])){
            $imperfectShift->reason = $_GET['reason'];
        }
        if(isset($_GET['other'])){
            $imperfectShift->other = $_GET['other'];
        }

        updateImperfectShift($imperfectShift);
        header(' ', true, 204);
        echo json_encode(['status' => 'success']);
        die();
        break;

    case '/api/performanceQuery':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            echo json_encode($resp);
            die();
        } else if (!isFsr()) {
            header(' ', true, 403);
            $resp['message'] = "Not Authorised";
            json_encode($resp);
            die();
        }

        //set start date to the last date we queried to
        $startDate = getLastQueriedDate();
        //set the end date to 2 weeks prior to todays date
        $endDate = date_create_from_format('Y-m-d H:i:s', date('Y-m-d H:i:s', time()));
        $endDate = date_sub($endDate, date_interval_create_from_date_string('1 days'));

        if($startDate >= $endDate){
            $resp = "Results up to date";
            echo json_encode($resp);
            die();
        }

        $holidays = getHolidays($startDate, $endDate);

        $employeeList = \timeclock\getEmployees()->data;
        //loop through all employees and run performance query on their hours
        //This adds all imperfect shifts for that employee to the database
        foreach($employeeList as $employee){
            $punchesForEmployee = \timeclock\getEmployeePunches($employee->employeeId, $startDate, $endDate)->data;
            performanceQuery($punchesForEmployee, fetchShiftsForEmployee($employee->employeeId, $startDate, $endDate), clone $startDate, clone $endDate, $holidays);
        }

        //bring the last queried date up to two weeks prior today
        updateLastQueriedDate($endDate);
        header(' ', true, 204);
        echo json_encode(['status' => 'success']);

        die();
        break;
    case '/api/holiday/remove':
        if(empty(trim($_POST['holidayId']))) {
            header(' ', true, 400);
            echo json_encode(['message' => 'Error: Holiday ID Not Specified']);
            die();
        }

        removeHoliday(intval($_POST['holidayId']));
        echo json_encode(['message' => 'Success']);
        die();
        break;

    case '/api/holiday/add':
        if(empty(trim($_POST['holidayName']))){
            header(' ', true, 400);
            $resp['message'] = "Holiday name not specified.";
            echo json_encode($resp);
            die();
        }
        if(empty(trim($_POST['startDate']))){
            header(' ', true, 400);
            $resp['message'] = "Start date not specified.";
            echo json_encode($resp);
            die();
        }
        $startDate = DateTime::createFromFormat('m/d/Y', $_POST['startDate']);
        if ($startDate == false) {
            header(' ', true, 400);
            echo json_encode(['message' => 'Start date in incorrect format']);
            die();
        }
        if(empty(trim($_POST['endDate']))){
            header(' ', true, 400);
            $resp['message'] = "End date not specified.";
            echo json_encode($resp);
            die();
        }
        $endDate = DateTime::createFromFormat('m/d/Y', $_POST['endDate']);
        if ($endDate == false) {
            header(' ', true, 400);
            echo json_encode(['message' => 'End date in incorrect format']);
            die();
        }

        $now = new DateTime('midnight');
        if ($startDate < $now) {
            header(' ', true, 400);
            echo json_encode(['message' => 'Start date in cannot be in the past']);
            die();
        }

        if ($startDate > $endDate) {
            header(' ', true, 400);
            echo json_encode(['message' => 'Start date may not be after end date']);
            die();
        }

        $holiday = new \Holiday();
        $holiday->holidayName = trim($_POST['holidayName']);
        $holiday->holidayStartDate = $startDate;
        $holiday->holidayEndDate = $endDate;

        $id = insertHoliday($holiday);
        header(' ', true, 204);
        echo json_encode(['message' => 'Success', 'holidayId' => $id]);
        die;
        break;

    case '/api/lab':
        if (isset($_GET['id'])) {
            $lab = getLabRequestById($_GET['id']);
            if ($lab == null) {
                header(' ', true, 404);
                $resp['message'] = 'Not Found';
                echo json_encode($resp);
                die();
            }
            echo json_encode($lab);
            die();
        } elseif (isset($_GET['start']) && isset($_GET['end'])) {

            //Parsing dates and times
            //Format:
            // 2018-03-12T00:00:00
            $start = date_create_from_format('Y-m-d\TH:i:s', $_GET['start']);
            if ($start === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }

            $end = date_create_from_format('Y-m-d\TH:i:s', $_GET['end']);
            if ($end === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }

        }
        $events = getReservationsByDateRange($start, $end);

        echo json_encode($events);
        die();
        break;

    /**
     * API Method For Creating A Lab Request
     */
    case '/api/lab/create':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            json_encode($resp);
            die();
        }

        //Checks if form is filled and if not throws error
        if (!isset($_POST['dept'])) {
            header(' ', true, 400);
            $resp['message'] = "No Department Provided";
            echo json_encode($resp);
            die();
        }

        if (!isset($_POST['reserving_class'])) {
            header(' ', true, 400);
            $resp['message'] = "No Class Number Provided";
            echo json_encode($resp);
            die();
        }

        if (!isset($_POST['reserving_section'])) {
            header(' ', true, 400);
            $resp['message'] = "No Class Section Provided";
            echo json_encode($resp);
            die();
        }

        if (!isset($_POST['lab'])) {
            header(' ', true, 400);
            $resp['message'] = "No Lab Type Provided";
            echo json_encode($resp);
            die();
        }

        if (!isset($_POST['date'])) {
            header(' ', true, 400);
            $resp['message'] = "No Date Provided";
            echo json_encode($resp);
            die();
        }
        elseif ($_POST['date'] != 'Invalid date') {
            $reservation_date = date_create_from_format('m-d-Y H:i', $_POST['date'] . " " . $_POST['startTime']);
        }
        else{
            $reservation_date = date_create_from_format('m/d/Y H:i', $_POST['startDate'] . " " . $_POST['startTimeRepeat']);
        }

        $today = date('m-d-Y', time());
        $today = date_create_from_format('m-d-Y',$today);

        //Enforces department reservation rules
        $diff = $today->diff($reservation_date);
        if(strtoupper($_POST['dept']) !== 'CIS') {
            if ($diff->d > $config['maxNonCISReservationDays']) {
                header(' ', true, 400);
                $resp['message'] = "Please select a date within two weeks of today";
                echo json_encode($resp);
                die();

            }
        }

        if($diff->invert == 1){
                header(' ', true, 400);
                $resp['message'] = "Please select a future date.";
                echo json_encode($resp);
                die();
        }

        if (!isset($_POST['startTime'])) {
            header(' ', true, 400);
            $resp['message'] = "No Start Time Provided";
            echo json_encode($resp);
            die();
        }

        if (!isset($_POST['endTime'])) {
            header(' ', true, 400);
            $resp['message'] = "No End Time Provided";
            echo json_encode($resp);
            die();
        }

        if (isset($_POST['reserving_class'])) {
            if (!is_numeric($_POST['reserving_class'])) {
                header(' ', true, 400);
                $resp['message'] = "Please enter a valid class number";
                echo json_encode($resp);
                die();
            }
        }

        $is_teaching_lab = $_POST['lab'] === "teaching";

        if(isset($_POST['repeatCheck'])) {

            $response = [];
            $repeating_lab = new \Repeating_Reservation();
            $repeating_lab->start_date = date_create_from_format('m/d/Y' ,$_POST['startDate']);
            $repeating_lab->end_date = date_create_from_format('m/d/Y' ,$_POST['endDate']);
            $repeating_lab->monday = (isset($_POST['check_mon']) && $_POST['check_mon'] === 'true');
            $repeating_lab->tuesday = (isset($_POST['check_tues']) && $_POST['check_tues'] === 'true');
            $repeating_lab->wednesday = (isset($_POST['check_wed']) && $_POST['check_wed'] === 'true');
            $repeating_lab->thursday = (isset($_POST['check_thurs']) && $_POST['check_thurs'] === 'true');
            $repeating_lab->friday = (isset($_POST['check_fri']) && $_POST['check_fri'] === 'true');
            $repeating_lab->reserving_class = $_POST['dept'] . " " . $_POST['reserving_class'] . " " . $_POST['reserving_section'];
            $repeating_lab->is_teaching_lab = $is_teaching_lab;
            $repeating_lab->start_time = date_create_from_format("H:i", $_POST['startTimeRepeat']);
            $repeating_lab->end_time = date_create_from_format("H:i", $_POST['endTimeRepeat']);

            if($repeating_lab->start_time === $repeating_lab->end_time) {
                header(' ', true, 400);
                $resp['message'] = "Please select a different start and end time";
                echo json_encode($resp);
                die();
            }
            if($repeating_lab->start_date > $repeating_lab->end_date) {
                header(' ', true, 400);
                $resp['message'] = "Please select an end date that is after the start date";
                echo json_encode($resp);
                die();
            }
            $date = clone $repeating_lab->start_date;

            //Ensures bad data is not allowed into the database
            if($repeating_lab->start_time > $repeating_lab->end_time) {
                header(' ', true, 400);
                $resp['message'] = "Please select an end time after the start time";
                echo json_encode($resp);
                die();
            }
            if (isset($_POST['repeating_lab_id']) && !empty($_POST['repeating_lab_id'])) {
                $temp_repeating_lab = getRepeatingReservationById($_POST['repeating_lab_id']);
                $repeating_lab->faculty_id = $temp_repeating_lab->reserving_faculty_id;
                _removeRepeatingReservation(getRepeatingReservationById($_POST['repeating_lab_id']));
            }
            $repeating_lab->repeating_res_id = _insertRepeatingReservation($repeating_lab);

            $createdReservations = [];
            $conflictDates = [];
            //Loops through days until the end date is reached
            //Determines the day of the week and if that day was selected
            while ($date <= $repeating_lab->end_date) {
                $lab = new \Lab();
                $dow = intval($date->format('w'));
                switch ($dow) {
                    case 1:
                        if ($repeating_lab->monday)
                            $lab->date = clone $date;
                        break;
                    case 2:
                        if ($repeating_lab->tuesday)
                            $lab->date = clone $date;
                        break;
                    case 3:
                        if ($repeating_lab->wednesday)
                            $lab->date = clone $date;
                        break;
                    case 4:
                        if ($repeating_lab->thursday)
                            $lab->date = clone $date;
                        break;
                    case 5:
                        if ($repeating_lab->friday)
                            $lab->date = clone $date;
                        break;
                    default:
                        // Make Sure Date Is Not Set If We Are Not On The Correct Date
                        $lab->date = null;
                }

                // If We're on the right day, save the reservation
                if (isset($lab->date)) {

                    // We're on the right day. make sure we don't conflict
                    if (checkReservationDates($date, $repeating_lab->start_time, $repeating_lab->end_time, $is_teaching_lab)) {
                        $conflictDates[] = clone $date;
                    } else { // No Conflict, Save the reservation
                        if(isset($repeating_lab->faculty_id)) {
                            $lab->faculty_id = $repeating_lab->faculty_id;
                        }
                        else {
                            $lab->faculty_id = $_SESSION['userId'];
                        }
                        $lab->dept = $_POST['dept'];
                        $lab->reserving_class = $_POST['reserving_class'];
                        $lab->is_teaching_lab = $is_teaching_lab;
                        $lab->start_time = $repeating_lab->start_time;
                        $lab->end_time = $repeating_lab->end_time;
                        $lab->reserving_section = $_POST['reserving_section'];
                        $lab->repeating_reservation_id = $repeating_lab->repeating_res_id;

                        $response[] = persistLab($lab);
                        $createdReservations[] = clone $lab;
                    }
                }

                /** @noinspection PhpUnhandledExceptionInspection */
                $date = $date->add(new DateInterval('P1D'));
                unset($lab);
            }

            if (count($conflictDates)) {
                $response['conflicts'] = array_map(function (DateTime $value) {
                    return $value->format('m-d-Y');
                }, $conflictDates);
            }

            if (count($createdReservations) === 0) {
                _removeRepeatingReservation($repeating_lab);
            }

            if ($config['enableEmailNotifications']) {
                try {
                    $email = getCurrentUserEmail();
                    $reservedLab = ($repeating_lab->is_teaching_lab) ? "Teaching Lab":"Outside Teaching Lab";
                    if (strlen($email) > 0) {
                        email\repeatingReservation($email,
                            $repeating_lab,
                            $reservedLab,
                            $createdReservations,
                            $conflictDates,
                            $config);
                    }

                } catch (\email\EmailException $e) {
                }
            }

        } else {

            //Parsing dates and times
            $parseDate = date_create_from_format('m-d-Y', $_POST['date']);
            if ($parseDate === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }
            $parseStart = date_create_from_format('H:i', $_POST['startTime']);
            if ($parseStart === false) {
                header(' ', true, 400);
                $resp['message'] = "Start Time Not Valid";
                echo json_encode($resp);
                die();
            }
            $parseEnd = date_create_from_format('H:i', $_POST['endTime']);
            if ($parseEnd === false) {
                header(' ', true, 400);
                $resp['message'] = "End Time Not Valid";
                echo json_encode($resp);
                die();
            }
            if ($parseStart == $parseEnd) {
                header(' ', true, 400);
                $resp['message'] = "Please select a different start and end time";
                echo json_encode($resp);
                die();
            }
            if ($parseStart > $parseEnd) {
                header(' ', true, 400);
                $resp['message'] = "Select an end time that is after your start time";
                echo json_encode($resp);
                die();
            }
            //Checks if a reservation is already in place for non-repeating reservations
            if (checkReservationDates(date_create_from_format("m-d-Y", $_POST['date']), $parseStart, $parseEnd, $is_teaching_lab)) {
                header(' ', true, 409);
                $resp['message'] = "Could not fulfill request, there is already a reservation in that time slot";
                echo json_encode($resp);
                die();
            }

            //Creates a new lab object to be passed into the query
            $lab = new \Lab();
            $lab->faculty_id = $_SESSION['userId'];
            $lab->dept = $_POST['dept'];
            $lab->reserving_class = $_POST['reserving_class'];
            $lab->is_teaching_lab = $is_teaching_lab;
            $lab->date = $parseDate;
            $lab->start_time = $parseStart;
            $lab->end_time = $parseEnd;
            $lab->reserving_section = $_POST['reserving_section'];

            $response['id'] = persistLab($lab);

            if ($config['enableEmailNotifications']) {
                try {
                    $email = getCurrentUserEmail();
                    if (strlen($email) > 0) {
                        email\singleReservation($email, $lab, $config);
                    }

                } catch (\email\EmailException $e) {
                }
            }
        }

        header(' ', true, 201);
        echo json_encode($response);
        die();
        break;

    case "/api/lab/fetch":
        if (!isset($_POST['lab_id'])) {
            header(' ', true, 400);
            echo json_encode(['message' => "lab_id Is Not Set"]);
            die();
        }
        $lab = getLabRequestById(intval($_POST['lab_id']));

        if (is_null($lab)) {
            header(' ', true, 404);
            echo json_encode(['message' => 'Lab Reservation Not Found']);
            die();
        }

        // Format For Client
        $lab->date = $lab->date->format('m/d/Y');
        $lab->start_time = $lab->start_time->format('g:ia');
        $lab->end_time = $lab->end_time->format('g:ia');

        echo json_encode($lab);
        die();
        break;
    case '/api/lab/update':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            json_encode($resp);
            die();
        }

        if (!isset($_POST['lab_id'])) {
            header(' ', true, 400);
            $resp['message'] = 'Id Not Provided';
            echo json_encode($resp);
            die();
        }

        $lab = getLabRequestById($_POST['lab_id']);
        if ($lab == null) {
            header(' ', true, 404);
            $resp['message'] = 'Not Found';
            echo json_encode($resp);
            die();
        }

        if (!isFsr() && $lab->faculty_id !== $_SESSION['userId']) {
            header(' ', true, 403);
            $resp['message'] = 'Unauthorized';
            echo json_encode($resp);
            die();
        }


        if (isset($_POST['lab_id']))
            $lab->reservation_id = $_POST['lab_id'];
        if (isset($_POST['dept']))
            $lab->dept = $_POST['dept'];
        if (isset($_POST['reserving_class']))
            $lab->reserving_class = $_POST['reserving_class'];
        if (isset($_POST['lab']))
            $lab->is_teaching_lab = $_POST['lab'];

        if (isset($_POST['date'])){
            $parseDate = date_create_from_format('m-d-Y', $_POST['date']);
            if ($parseDate === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }
            $lab->date = $parseDate;
        }

        if (isset($_POST['startTime'])) {
            $parseStart = date_create_from_format('H:i', $_POST['startTime']);
            if ($parseStart === false) {
                header(' ', true, 400);
                $resp['message'] = "Start Time Not Valid";
                echo json_encode($resp);
                die();
            }
            $lab->start_time = $parseStart;
        }

        if (isset($_POST['endTime'])) {
            $parseEnd = date_create_from_format('H:i', $_POST['endTime']);
            if ($parseEnd === false) {
                header(' ', true, 400);
                $resp['message'] = "End Time Not Valid";
                echo json_encode($resp);
                die();
            }
            $lab->end_time = $parseEnd;
        }

        $response['id'] = persistLab($lab);

        if ($config['enableEmailNotifications']) {
            try {
                $email = getCurrentUserEmail();
                if (strlen($email) > 0) {
                    email\UpdateSingleReservation($email, $lab, $config);
                }

            } catch (\email\EmailException $e) {
            }
        }
        echo json_encode($response);
        die();
        break;

    case '/api/lab/delete':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = 'Not Logged In';
            echo json_encode($resp);
            die();
        }
        $lab = getLabRequestById(intval($_POST['lab_id']));
        if ($lab == null || $lab->reservation_id == null) {
            header(' ', true, 400);
            die();
        }

        if (!isFsr() && $lab->faculty_id !== $_SESSION['userId']) {
            header(' ', true, 403);
            $resp['message'] = 'Unauthorized';
            echo json_encode($resp);
            die();
        }

        _removeLabReservation($lab);
        $resp['id'] = $_POST['lab_id'];
        echo json_encode($resp);
        break;

    case "/api/repeating_reservation/delete":
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = 'Not Logged In';
            echo json_encode($resp);
            die();
        }
        $lab = getRepeatingReservationById($_POST['repeating_reservation_id']);
        if ($lab == null || $lab->repeating_res_id == null) {
            header(' ', true, 400);
            die();
        }

        if (!isFsr() && $lab->reserving_faculty_id !== $_SESSION['userId']) {
            header(' ', true, 403);
            $resp['message'] = 'Unauthorized';
            echo json_encode($resp);
            die();
        }
        _removeRepeatingReservation($lab);
        $resp['id'] = $_POST['repeating_reservation_id'];
        echo json_encode($resp);
        break;

    case "/api/repeating_reservation/fetch":
        if (!isset($_POST['repeating_reservation_id'])) {
            header(' ', true, 400);
            echo json_encode(['message' => "repeating_reservation_id Is Not Set"]);
            die();
        }

        $lab = getRepeatingReservationById(intval($_POST['repeating_reservation_id']));

        if (is_null($lab)) {
            header(' ', true, 404);
            echo json_encode(['message' => 'Repeating Lab Reservation Not Found']);
            die();
        }

        $startDate = date_create_from_format("Y-m-d", $lab->start_date);
        $lab->start_date = $startDate->format("m/d/Y");
        $endDate = date_create_from_format('Y-m-d', $lab->end_date);
        $lab->end_date = $endDate->format('m/d/Y');
        $lab->start_time = $lab->start_time->format('h:ia');
        $lab->end_time = $lab->end_time->format('h:ia');
        echo json_encode($lab);
        die();
        break;


    /**
     * API Method For Creating A User
     * A User May Only Be Created By An FSR Member
     *
     * @method POST
     *
     * @parameter username {string}
     *  Unique Email of The New User
     *
     * @parameter password {string}
     *  The Password For The New User
     *
     * @parameter role {string} (fsr|faculty)
     *  The Role of The New User
     *
     * @return
     *  201:
     *      The User Was Successfully Created
     *      json: { id: (the ID of The New User) }
     *  400:
     *      Some Field Was Not Provided/Malformed
     *  401:
     *      The User Is Not Logged In
     *  403:
     *      The User Is Not Authorized To Create A User
     *  409:
     *      The username Is Already In Use
     */
    case '/api/user/add':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            json_encode($resp);
            die();
        } else if (!isFsr()) {
            header(' ', true, 403);
            $resp['message'] = "Not Authorised";
            json_encode($resp);
            die();
        }

        if (!isset($_POST['username'])) {
            header(' ', true, 400);
            echo json_encode(['message' => 'No Username Provided']);
            die();
        }

        if (!isset($_POST['host'])) {
            header(' ', true, 400);
            echo json_encode(['message' => 'No Host Provided']);
            die();
        }

        if (!isset($_POST['email'])) {
            header(' ', true, 400);
            echo json_encode(['message' => 'No Email Provided']);
            die();
        }

        if (!isset($_POST['password']) && $_POST['host'] === 'localhost') {
            header(' ', true, 400);
            echo json_encode(['message' => 'No password Provided']);
            die();
        }

        if (!isset($_POST['role'])) {
            header(' ', true, 400);
            echo json_encode(['message' => 'No Role Provided']);
            die();
        }
        if (!isset($_POST['isActive'])) {
            header(' ', true, 400);
            echo json_encode(['message' => 'isActive Not Set']);
            die();
        }

        $existingUser = getUserByUsername($_POST['username']);
        if (!is_null($existingUser)) {
            header(' ', true, 409);
            echo json_encode(['message' => 'Username Already Taken']);
            die();
        }

        if ($_POST['host'] === 'localhost')
            $id = createLocalUser($_POST['username'], $_POST['email'],  $_POST['password'], $_POST['role'], $_POST['isActive'] === "true");
        else if ($_POST['host'] === 'clarion.edu') {
            $id = createClarionUser($_POST['username'], $_POST['email'], $_POST['role'], $_POST['isActive'] === "true");
        }
        else {
            header(' ', true, 400);
            echo json_encode(['message' => 'Unrecognised Host']);
            die();
        }

        if ($id == 0) {
            header(' ', true, 400);
            echo json_encode(['message' => 'Failed To Create User']);
            die();
        }

        header(' ', true, 201);
        echo json_encode(['id' => $id]);
        break;

    /**
     * API Method For Updating A User
     * A User May Only Be Updated By An FSR Member
     *
     * @method POST
     *
     * @parameter [username] {string}
     *  Unique Email of The New User
     *
     * @parameter [password] {string}
     *  The Password For The New User
     *
     * @parameter [role] {string} (fsr|faculty)
     *  The Role of The New User
     *
     * @return
     *  200:
     *      The User Was Successfully Updated
     *      json: { id: (the ID of The Updated User) }
     *  400:
     *      Some Field Was Not Provided/Malformed
     *  404:
     *      The User Was Not Found
     *  401:
     *      The User Is Not Logged In
     *  403:
     *      The User Is Not Authorized To Update A User
     *  409:
     *      The username Is Already In Use
     */
    case '/api/user/update':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            json_encode($resp);
            die();
        } else if (!isFsr()) {
            header(' ', true, 403);
            $resp['message'] = "Not Authorised";
            json_encode($resp);
            die();
        }
        if (!isset($_POST['id'])) {
            header(' ', true, 404);
            $resp['message'] = "No ID Provided";
            json_encode($resp);
            die();
        }

        $user = getUserById($_POST['id']);
        if (is_null($user)) {
            header(' ', true, 404);
            $resp['message'] = "User Not Found";
            echo json_encode($resp);
            die();
        }

        if (isset($_POST['username'])) {
            $existingUser = getUserByUsername($_POST['username']);
            if ($existingUser->id != null && $existingUser->id != $user->id) {
                header(' ', true, 409);
                echo json_encode(['message' => 'Username Already Used']);
                die();
            }
            $user->username = $_POST['username'];
        }

        if (isset($_POST['host'])) {
            if ($_POST['host'] !== 'clarion.edu' && $_POST['host'] !== 'localhost') {
                header(' ', true, 400);
                echo json_encode(['message' => 'Unrecognised Host']);
                die();
            }

            $user->host = $_POST['host'];
        }

        if (isset($_POST['email']))
            $user->email = $_POST['email'];

        if (isset($_POST['password']) && $user->host !== 'clarion.edu')
            $user->hashedPass = _generatePassword($_POST['password'], $user->salt);

        // Clear Passwords On Clarion Users
        if ($user->host === 'clarion.edu') {
            $user->hashedPass = null;
            $user->salt = null;
        }

        if (isset($_POST['role']))
            $user->role = $_POST['role'];

        if (isset($_POST['isActive']))
            $user->isActive = $_POST['isActive'] === "true";

        echo json_encode(['id' => persistUser($user)]);
        break;
    /**
     * API Method For Retrieving One User / All Users
     * May Only Be Invoked By FSR Members
     *
     * @method GET
     *
     * @parameter [id] {int}
     *  ID of A User To Retrieve
     *
     * @return
     *  200:
     *      One/All Users Were JSON Encoded And Returned
     *          One: {id, username, is_active, role}
     *          All: json: array({id, username, is_active, role})
     *  401:
     *      User Is Not Logged In
     *  403:
     *      User Is Not An FSR Member
     */
    case '/api/users':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            die();
        }

        if (!isFsr()) {
            header(' ', true, 403);
            die();
        }

        if (isset($_GET['id'])) {
            echo json_encode(getReducedUserById($_GET['id']));
            die();
        }

        echo json_encode(getAllUsers());
        die();
        break;
    /**
     * API Method To Change The Password of The Current User
     *
     * @method POST
     *
     * @parameter password {string}
     *  The New Password
     *
     * @return
     *  200:
     *      The User Was Successfully Updated
     *      json: { id: (the ID of The Updated User) }
     *  400:
     *      A Password Was Not Provided
     *  404:
     *      The User Was Not Found
     *  401:
     *      The User Is Not Logged In
     */
    case '/api/user/changePass':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            die();
        }
        $currentUser = getUserById($_SESSION['userId']);
        if (is_null($currentUser)) {
            header(' ', true, 404);
            die();
        }
        if (!isset($_POST['password']) || strlen($_POST['password']) == 0) {
            header(' ', true, 400);
            $resp['message'] = 'Password Required';
            json_encode($resp);
            die();
        }

        $currentUser->hashedPass = _generatePassword($_POST['password'], $currentUser->salt);
        $resp['id'] = persistUser($currentUser);
        echo json_encode($resp);
        break;
    /**
     * Server A userForm For A User
     * Only A FSR Member May Access This Page
     *
     * @method GET
     *
     * @parameter id {int}
     *  ID of The User To Populate The Form With
     *
     * @return
     *  200 + View:
     *      The Form Was Successfully Loaded
     *  401:
     *      User Is Not Logged In
     *  404:
     *      The User Was Not Found
     *  403:
     *      User Is Not An FSR Member
     */
    case '/user/userForm':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }
        if (!isFsr()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }
        if (!isset($_GET['id'])) {
            header(' ', true, 404);
            die();
        }
        $user = getUserById($_GET['id']);
        if (is_null($user)) {
            header(' ', true, 404);
            die();
        }
        require_once 'user/userForm.php';
        break;
    /**
     * Server A Table of All Users
     * Only A FSR Member May Access This Page
     *
     * @return
     *  200 + View:
     *      The User Is Authorised To See This Page
     *  401:
     *      User Is Not Logged In
     *  403:
     *      User Is Not An FSR Member
     */
    case '/user/all':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isFsr()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }
        require_once 'user/allUsers.php';
        break;
    /**
     * Server A userForm For Creating A User
     * Only A FSR Member May Access This Page
     *
     * @return
     *  200 + View:
     *      The Form Was Successfully Loaded
     *  401:
     *      User Is Not Logged In
     *  403:
     *      User Is Not An FSR Member
     */
    case '/userForm':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isFsr()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }

        require_once 'user/userForm.php';
        break;
    /**
     * Server A Form For Updating The Current User's Password
     * Any Logged In User May Access This Page
     *
     * @return
     *  200 + View:
     *      The Form Was Successfully Loaded
     *  401:
     *      User Is Not Logged In
     */
    case '/user/changeUserPassword':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }
        require_once 'user/changeUserPassword.php';
        break;
    /**
     * Server A Form For Requesting Drives
     * Any Logged In User May Access This Page
     *
     * @return
     *  200 + View:
     *      The Form Was Successfully Loaded
     *  401:
     *      User Is Not Logged In
     */
    case '/drive/driveRequestForm':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }
        require_once 'drive/driveRequestForm.php';
        break;
    /**
     * Server A Form For Editing A request
     * Only The Creator Of The Request or A FSR Member May Access This Page
     *
     * @method GET
     *
     * @parameter id {int}
     *  ID of The Request To Populate The Form With
     *
     * @return
     *  200 + View:
     *      The Form Was Successfully Loaded
     *  401:
     *      User Is Not Logged In
     *  403:
     *      User Is Not The Creator of The Request Or A FSR Member
     *  404:
     *      The Request Was Not Found
     */
    case '/request/edit':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        $request = getRequestById($_GET['id']);
        if ($request == null) {
            header(' ', true, 404);
            die();
        }
        if (!isFsr() && $request->userId !== $_SESSION['userId']) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }
        require_once 'drive/driveRequestForm.php';
        break;
    /**
     * Server A View of A request
     * Only The Creator Of The Request or A FSR Member May Access This Page
     *
     * @method GET
     *
     * @parameter id {int}
     *  ID of The Request To Populate The View With
     *
     * @return
     *  200 + View:
     *      The View Was Successfully Loaded
     *  401:
     *      User Is Not Logged In
     *  403:
     *      User Is Not The Creator of The Request Or A FSR Member
     *  404:
     *      The Request Was Not Found
     */
    case '/request':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isset($_GET['id'])) {
            header(' ', true, 404);
            die();
        }
        $request = getRequestById($_GET['id']);
        if ($request == null || $request->id == null) {
            header(' ', true, 404);
            die();
        }
        if (!isFsr() && $request->userId !== $_SESSION['userId']) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }
        require_once 'drive/viewDriveRequest.php';
        break;
    /**
     * Shows The Current Lab Schedule
//     * Anyone May Access This Page
     */
    case '/schedule':
        $employeeResp = timeclock\getEmployees();

        if (isset($employeeResp->data)) {
            $employeeMap = mapEmployees($employeeResp);
        } else {
            $errorMessage = "Failed to retrieve employees";
            require_once 'error.php';
            die();
        }

        $activeSchedule = getActiveScheduleForDate();
        if ($activeSchedule) {
            $shifts = getRawEmployeeShiftsBySchedule($activeSchedule->scheduleNumber);
        } else {
            $shifts = [];
        }


        $labShiftHours = [];
        $fsrShiftHours = [];
        foreach ($shifts as $shift) {
            $index = $shift->dayOfShift . ':' . (intval($shift->shiftStart->format('H') ) + 1);

            if ($shift->type === 'LAB') {
                if (isset($labShiftHours[$index])) {
                    $labShiftHours[$index][] = $shift;
                } else {
                    $labShiftHours[$index] = [$shift];
                }
            } else if ($shift->type === 'FSR') {
                if (isset($fsrShiftHours[$index])) {
                    $fsrShiftHours[$index][] = $shift;
                } else {
                    $fsrShiftHours[$index] = [$shift];
                }
            }
        }

        require_once 'employee/schedule.php';
        break;
    /**
     * Server A View of Requests
     *  If The User Is faculty, Then only Their Requests Are Shown
     *  If The User Is FSR, Then All Requests Will Be Shown
     *
     *
     * @return
     *  200 + View:
     *      The View Was Successfully Loaded
     *  401:
     *      User Is Not Logged In
     */
    case '/drive/driveRequests':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }
        require_once 'drive/driveRequests.php';
        break;
    case '/chicken':
        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=chicken.pdf");
        @readfile('node_modules/cnn/chicken.pdf');
        break;
    case '/employee/scheduleEditor':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isFsr()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }

        $employeeResp = timeclock\getEmployees();

        if (isset($employeeResp->data)) {
            $employeeMap = mapEmployees($employeeResp);
        } else {
            $errorMessage = "Failed to retrieve employees";
            require_once 'error.php';
            die();
        }

        $activeSchedule = getActiveScheduleForDate();
        if ($activeSchedule) {
            $shifts = getRawEmployeeShiftsBySchedule($activeSchedule->scheduleNumber);
        } else {
            $shifts = [];
        }


        $labShiftHours = [];
        $fsrShiftHours = [];
        foreach ($shifts as $shift) {
            $index = $shift->dayOfShift . ':' . (intval($shift->shiftStart->format('H') ) + 1);

            if ($shift->type === 'LAB') {
                if (isset($labShiftHours[$index])) {
                    $labShiftHours[$index][] = $shift;
                } else {
                    $labShiftHours[$index] = [$shift];
                }
            } else if ($shift->type === 'FSR') {
                if (isset($fsrShiftHours[$index])) {
                    $fsrShiftHours[$index][] = $shift;
                } else {
                    $fsrShiftHours[$index] = [$shift];
                }
            }
        }

        require_once 'employee/scheduleEditor.php';
        break;
    case '/employee/performance':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isFsr()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }
        require_once 'employee/performance.php';
        break;
    case '/lab/labRequests':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isFsr() && !isFaculty()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }

        $individualReservations = _getLabReservationsWithReservingFaculty($_SESSION['userId']);
        $repeatingReservations = _getRepeatingReservationsWithReservingFaculty($_SESSION['userId']);
        require_once 'lab/labRequests.php';
        die();
        break;
    case '/export/imperfectshift':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            $resp['message'] = "Not Logged In";
            json_encode($resp);
            die();
        } else if (!isFsr()) {
            header(' ', true, 403);
            $resp['message'] = "Not Authorised";
            json_encode($resp);
            die();
        }

        if (isset($_GET['startDate'])) {
            $startDate = date_create_from_format('Y-m-d', $_GET['startDate']);
            if ($startDate === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }
        }
        else{
            $startDate = null;
        }
        if (isset($_GET['endDate'])) {
            $endDate = date_create_from_format('Y-m-d', $_GET['endDate']);
            if ($endDate === false) {
                header(' ', true, 400);
                $resp['message'] = "Date Not Valid";
                echo json_encode($resp);
                die();
            }
        }
        else{
            $endDate = null;
        }

        if (isset($_GET['employeeId'])) {
            $employeeId = intval($_GET['employeeId']);
            if (!is_int($employeeId)) {
                header(' ', true, 400);
                $resp['message'] = "Employee ID Not Valid";
                echo json_encode($resp);
                die();
            }
        } else {
            $employeeId = 0;
        }


        //Outputs imperfect shifts as CSV file
        header("Content-type: text/csv");
        header("Cache-Control: no-store, no-cache");
        header('Content-Disposition: attachment; filename="report.csv"');
        $file = fopen('php://output','w');

        try {
            $imperfectShifts = getImperfectShiftInfo($employeeId, $startDate, $endDate);
            $employees = \timeclock\getEmployees()->data;
            foreach($imperfectShifts as $imperfectShift) {
                foreach($employees as $employee){
                    if(intval($imperfectShift->employeeId) == $employee->employeeId){
                        $imperfectShift->firstName = $employee->firstName;
                        $imperfectShift->lastName =$employee->lastName;
                    }
                }
            }
            // Put Headers
            fputcsv($file, ["First Name", "Last Name", "Date",
                "Shift", "Type", "Minutes", "Excused", "Reason", "Other"]);

            // Put Data
            foreach ($imperfectShifts as $shift) {
                fputcsv($file, [
                    "First Name" => $shift->firstName,
                    "Last Name" => $shift->lastName,
                    "Date" => $shift->shiftDate->format('m/d/Y'),
                    "Shift" => $shift->shiftTime,
                    "Type" => $shift->type,
                    "Minutes" =>  $shift->mins,
                    "Excused" => ($shift->excused) ? "Yes" : "No",
                    "Reason" => $shift->reason,
                    "Other" => $shift->other
                ]);
            }
        } catch (Exception $e) {
            echo json_encode([ "Error" => "Failed To Retrieve Employees"]);
            header(' ', true, 500);
        }
        break;
    case '/employee/holiday':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isFsr()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }
        require_once 'employee/holiday.php';
        break;
    case '/employee/employmentRecords':
        if (!isLoggedIn()) {
            header(' ', true, 401);
            require 'login.php';
            die();
        }

        if (!isFsr()) {
            header(' ', true, 403);
            require 'unauthorized.php';
            die();
        }
        require_once 'employee/employmentRecords.php';
        break;
    case '/license':
        require_once 'license.php';
        break;
    default:
        require_once 'home.php';
        break;
}

/**
 * Sets Fields, Generates A Salt, And Persists A User
 *
 * @param string $username {string}
 *  Unique Username/Email For The User
 *
 * @param string $email
 * Email To Send Notifications
 *
 * @param string $password {string}
 *  Password For The New User
 *
 * @param string $role {string}
 * (fsr|faculty)
 *  Role of The New User
 *
 * @param bool $isActive {bool}
 *  Flag If The User Is Active Or Not
 *
 * @return int
 *  ID of The New User
 *  0 May Indicate An Error
 */
function createLocalUser(string $username, string $email, string $password, string $role, bool $isActive) {
    $user = new \User();
    $user->username = $username;
    $user->host = 'localhost';
    $user->email = $email;

    $salt = openssl_random_pseudo_bytes(32);
    $user->salt = $salt;
    $hashedPass = _generatePassword($password, $salt);
    $user->hashedPass = $hashedPass;

    $user->isActive = $isActive;
    $user->role = $role;

    return persistUser($user);
}

/**
 * Sets Fields, And Persists A User
 *
 * @param $username {string}
 *  Unique Username For The User
 *
 * @param string $email
 * Email To Send Notifications
 *
 * @param $role {string} (fsr|faculty)
 *  Role of The New User
 *
 * @param $isActive {bool}
 *  Flag If The User Is Active Or Not
 *
 * @return int
 *  ID of The New User
 *  0 May Indicate An Error
 */
function createClarionUser(string $username, string $email, string $role, bool $isActive): int {
    $user = new User();
    $user->username = $username;
    $user->email = $email;

    // We Don't Store Clarion Users Passwords
    // So Password & Salt are Omitted

    $user->host = "clarion.edu";
    $user->isActive = $isActive;
    $user->role = $role;
    return persistUser($user);
}

//Buckle up, Buckaroo, this is where the magic happens
function performanceQuery($punches, $shifts, \DateTime $startDate, \DateTime $endDate, $holidays){

    //start the comparing at the date of the first punch
    $currentDate = clone $startDate;
    $currentDate->setTime(0,0);

    if($currentDate == false){
        echo 'No shifts were returned for this employee';
        return;
    }

    foreach($shifts as $shiftsOfSchedule) {
        if ($currentDate < $shiftsOfSchedule['schedule']->startActiveDate)
            $currentDate = clone $shiftsOfSchedule['schedule']->startActiveDate;

        while ($currentDate < $shiftsOfSchedule['schedule']->endActiveDate) {
            if($currentDate > $endDate){
                break;
            }
            $shiftDayOfWeek = intval($currentDate->format('w'));

            $skipShiftsOfDay = false;
            foreach($holidays as $holiday){
                $holidayStartDate = strtotime($holiday->holidayStartDate->format('Y-m-d'));
                $holidayEndDate = strtotime($holiday->holidayEndDate->format('Y-m-d'));
                $todayDate = strtotime($currentDate->format('Y-m-d'));
                if ($holidayStartDate <= $todayDate && $todayDate <= $holidayEndDate ) {
                    $skipShiftsOfDay = true;
                    break;
                }
            }
            if ($skipShiftsOfDay) {
                $currentDate->add(new DateInterval('P1D'));
                continue;
            }

            $shiftsOfDay = [];
            if (isset($shiftsOfSchedule['shifts'][intval($currentDate->format('w'))])) {
                $shiftsOfDay = $shiftsOfSchedule['shifts'][intval($currentDate->format('w'))];
            }

            foreach($shiftsOfDay as $shift){
                $shiftMissed = true;
                foreach($punches as $punch){

                    // Move Up To The Punches On The Right Date
                    if($punch->timeIn->format('Y-m-d') != $currentDate->format('Y-m-d')){
                        continue;
                    }



                    $punchTimeIn = strtotime($punch->timeIn->format('H:i'));
                    $shiftTimeIn = strtotime($shift->shiftStart->format('H:i'));
                    $punchTimeOut = strtotime($punch->timeOut->format('H:i'));
                    $shiftTimeOut = strtotime($shift->shiftEnd->format('H:i'));


                    if($punchTimeIn > $shiftTimeOut || $punchTimeOut < $shiftTimeIn){
                        continue;
                    }
                    $shiftMissed = false;
                    if($punchTimeIn > $shiftTimeIn) {
                        insertImperfectShift(makeLateImperfectShift($punch, $shift));
                    }
                    if($punchTimeOut < $shiftTimeOut){
                        insertImperfectShift(makeEarlyImperfectShift($punch, $shift));
                    }
                }
                if($shiftMissed){
                    insertImperfectShift(makeMissedImperfectShift($shift, $currentDate));
                }
            }
            $currentDate->add(new DateInterval('P1D'));
        }
    }
}
function makeLateImperfectShift($punch, $shift): ImperfectShift {
    $punchTime = strtotime($punch->timeIn->format('H:i'));
    $shiftTime = strtotime($shift->shiftStart->format('H:i'));
    $minutesDiff = floor( abs($shiftTime - $punchTime) / 60);

    $imperfectShift = new ImperfectShift();
    $imperfectShift->employeeId = $punch->employeeId;
    $imperfectShift->shiftNumber = $shift->shiftNumber;
    $imperfectShift->mins = $minutesDiff;
    $imperfectShift->timeclockShiftId = $punch->recordId;
    $imperfectShift->shiftDate = $punch->timeIn;
    $imperfectShift->type = 'Late';

    return $imperfectShift;
}

function makeEarlyImperfectShift($punch, $shift): ImperfectShift {
    $punchTime = strtotime($punch->timeOut->format('H:i'));
    $shiftTime = strtotime($shift->shiftEnd->format('H:i'));
    $minutesDiff = floor( abs($punchTime - $shiftTime) / 60);

    $imperfectShift = new ImperfectShift();
    $imperfectShift->employeeId = $punch->employeeId;
    $imperfectShift->shiftNumber = $shift->shiftNumber;
    $imperfectShift->mins = $minutesDiff;
    $imperfectShift->timeclockShiftId = $punch->recordId;
    $imperfectShift->shiftDate = $punch->timeIn;
    $imperfectShift->type = 'Out Early';

    return $imperfectShift;
}

function makeMissedImperfectShift($shift, $date): ImperfectShift {
    $imperfectShift = new ImperfectShift();
    $imperfectShift->employeeId = $shift->employeeId;
    $imperfectShift->shiftNumber = $shift->shiftNumber;
    $imperfectShift->mins = null;
    $imperfectShift->timeclockShiftId = null;
    $imperfectShift->shiftDate = $date;
    $imperfectShift->type = 'Missed';

    return $imperfectShift;
}
/**
 * Fetches Relevant Schedules In The Following Structure
 *
 * [
 *  (scheduleId) [
 *    schedule (Schedule)
 *    shifts: [
 *      3 (day of week starting at  Sunday = 0) [
 *          (EmployeeShifts)
 *      ]
 *    ]
 *  ]
 *
 * ]
 */
function fetchShiftsForEmployee ($employeeId, $startDate, $endDate) {
    $relevantSchedules = getSchedulesByDates($startDate, $endDate);

    $shiftsForEmployee = [];
    foreach($relevantSchedules as $schedule) {
        $shiftsForEmployee[$schedule->scheduleNumber] = ['schedule' => $schedule, 'shifts' => [] ];

        $temp = getEmployeeShifts($schedule->scheduleNumber, $employeeId);
        $dayOfWeek = -1;
        foreach($temp as $shift) {
            if (intval($shift->dayOfShift) == $dayOfWeek) {
                array_push($shiftsForEmployee[$schedule->scheduleNumber]['shifts'][$shift->dayOfShift], $shift);
            } else {
                $shiftsForEmployee[$schedule->scheduleNumber]['shifts'][$shift->dayOfShift] = [$shift];
                $dayOfWeek = intval($shift->dayOfShift);
            }
        }
    }

    return $shiftsForEmployee;
}
