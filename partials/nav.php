<nav class="navbar navbar-expand-md navbar-dark mb-4 bg-company-gold">
    <a class="navbar-brand" href="<?=$config['webRoot']?>index.php?path=/home">Becker Lab</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?=$config['webRoot']?>index.php?path=/schedule">Schedule</a>
            </li>
            <?php if (isFsr()) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$config['webRoot']?>index.php?path=/user/all">Users</a>
                </li>
            <?php } ?>

            <?php if (isFsr()) { ?>
            <li class="nav-item dropdown ">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Employee
                </a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?=$config['webRoot']?>index.php?path=/employee/scheduleEditor">Edit Schedule</a>
                    <a class="dropdown-item" href="<?=$config['webRoot']?>index.php?path=/employee/holiday">Holidays</a>
                    <a class="dropdown-item" href="<?=$config['webRoot']?>index.php?path=/employee/performance">Employee Performance</a>
                    <a class="dropdown-item" href="<?=$config['webRoot']?>index.php?path=/employee/employmentRecords">Employment Records</a>
                </div>
            </li>
            <?php } ?>

            <?php if (isFaculty() || isFsr()) { ?>
                <li class="nav-item dropdown ">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Drives
                    </a>
                    <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?=$config['webRoot']?>index.php?path=/drive/driveRequestForm">Request Drives</a>
                        <a class="dropdown-item"  href="<?=$config['webRoot']?>index.php?path=/drive/driveRequests">View Drive Requests</a>
                    </div>
                </li>
            <?php } ?>

            <?php if (isFaculty() || isFsr()) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?=$config['webRoot']?>index.php?path=/lab/labRequests">Request Lab</a>
            </li>
            <?php } ?>


        </ul>
        <!--Right-->
        <ul class="navbar-nav mt-2 mt-md-0">
            <?php if (isLoggedIn()) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$config['webRoot']?>index.php?path=/logout">Log Out</a>
                </li>
            <?php } else { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?=$config['webRoot']?>index.php?path=/login">Login</a>
                </li>
            <?php }?>
        </ul>
    </div>
</nav>