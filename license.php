<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Licenses</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>

</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container align-content-center align-items-center">
    <h3 >Licenses Used</h3>
    <table class="table table-striped table-sm table-responsive-sm">
        <thead>
            <tr>
                <th>Library</th>
                <th>License</th>
                <th>Link</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>jQuery + JQuery UI</td>
                <td>MIT</td>
                <td>
                    <a href="https://jquery.org/license/">https://jquery.org/license/</a>
                </td>
            </tr>
            <tr>
                <td>Bootstrap 4</td>
                <td>MIT</td>
                <td>
                    <a href="https://getbootstrap.com/docs/4.0/about/license/">https://getbootstrap.com/docs/4.0/about/license/</a>
                </td>
            </tr>
            <tr>
                <td>Dragula</td>
                <td>MIT</td>
                <td>
                    <a href="https://github.com/bevacqua/dragula/blob/master/license ">https://github.com/bevacqua/dragula/blob/master/license </a>
                </td>
            </tr>
            <tr>
                <td>php-curl</td>
                <td>MIT</td>
                <td>
                    <a href="https://github.com/anlutro/php-curl/blob/master/LICENSE">https://github.com/anlutro/php-curl/blob/master/LICENSE</a>
                </td>
            </tr>
            <tr>
                <td>Moment.js</td>
                <td>MIT</td>
                <td>
                    <a href="https://momentjs.com/">https://momentjs.com/</a>
                </td>
            </tr>
            <tr>
                <td>FullCalendar (Standard)</td>
                <td>MIT</td>
                <td>
                    <a href="https://fullcalendar.io/license">https://fullcalendar.io/license</a>
                </td>
            </tr>
            <tr>
                <td>Font Awesome (Free)</td>
                <td>(Mixed)</td>
                <td>
                    <a href="https://fontawesome.com/license">https://fontawesome.com/license</a>
                </td>
            <tr>
        </tbody>
    </table>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>