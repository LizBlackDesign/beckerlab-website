<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <title>Unauthorized</title>
</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container_replacement">
    <div class="titleCentering">
        <h1>Unauthorized</h1>
        <h5>You do not have access to this page.</h5>
        <div>
            <img class="img-fluid"  style="max-width: 25%" src="img/Red-x.png"/>
        </div>
    </div>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>
