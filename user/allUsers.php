<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Requests</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <script>

        window.addEventListener('load', function() {
            var table = $("#userTable");
            $.ajax({
                url:"<?=$config['webRoot']?>index.php?path=/api/users",
                dataType: 'json',
                data: {
                    status: 'Open'
                },
                statusCode: {
                    401: status401Handler
                }
            }).done(function (data, status, xhr) {
                var accumulator = table.html();

                for (var i = 0; i < data.length; i++) {
                    var user = data[i];
                    accumulator += "<tr>" +
                        "<td><a href=" + "'<?=$config['webRoot']?>index.php?path=/user/userForm&id=" + user.id + "'>" + user.username + "</a></td>" +
                        "<td>" + user.host + "</td>"+
                        "<td>" + user.role + "</td>" +
                        "<td>" + user.email + "</td>" +
                        "</tr>";
                }
                table.html(accumulator);
            })
        }, false);

    </script>
</head>
<body>

<?php require_once $config['serverRoot'] .'/partials/nav.php' ?>

<main role="main" class="container">
    <table id="userTable" class="table table-striped table-responsive-sm">
        <thead>
        <tr>
            <th>Username</th>
            <th>Host</th>
            <th>Role</th>
            <th>Email</th>
        </tr>
        </thead>
    </table>
    <div class="form-group">
        <a class="btn btn-lg btn-success" href="<?=$config['webRoot']?>index.php?path=/userForm">Add User</a>
    </div>
</main>
<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>