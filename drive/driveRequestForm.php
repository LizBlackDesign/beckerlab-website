<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Request Drives</title>

    <?php require_once $config['serverRoot'] .'/partials/scriptAndCss.php' ?>

    <script>
        function submitUpdate(data, done, fail) {
            $.ajax({
                type:'POST',
                url:'<?=$config['webRoot']?>index.php?path=/api/requests/update',
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
                done(data, textStatus, jqXHR);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                fail(jqXHR, textStatus, errorThrown);
            })
        }

        function submitAdd(data, done, fail) {
            $.ajax({
                type:'POST',
                url:'<?=$config['webRoot']?>index.php?path=/api/requests/create',
                dataType:'json',
                statusCode: {
                    401: status401Handler
                },
                data: data
            }).done(function (data, textStatus, jqXHR) {
                done(data, textStatus, jqXHR);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                fail(jqXHR, textStatus, errorThrown);
            })
        }

        $(document).ready(function() {
            var form = $("#driveForm");

            form.on('submit', function(event) {
                // Prevent Chrome Default Behavior of Submitting A From Whenever Any Button Is Clicked
                event.preventDefault();
                event.stopPropagation();

                form.addClass('was-validated');

                // Stop Invalid Data From Submitting
                if (form[0].checkValidity() === false)
                    return;

                var data = {
                    <?php if (isset($request)) { // Only Echo ID if We're Editing?>
                    id: <?php echo $request->id ?>,
                    <?php } ?>

                    driveClass: $("#forClass").val().trim(),
                    drives: $("#numberOfDrives").val(),
                    operatingSystem: $("#operatingSystem").val().trim(),
                    description: $("#softwareAndOtherRequirements").val()
                };

                var onSuccess = function (data, textStatus, jqXHR) {
                    window.location.replace('<?=$config['webRoot']?>index.php?path=/drive/driveRequests');
                };

                var onFail = function (jqXHR, textStatus, errorThrown) {
                    baseAjaxErrorHandler(jqXHR);
                };

                <?php if (isset($request)) { ?>
                submitUpdate(data, onSuccess, onFail);
                <?php } else { ?>
                submitAdd(data, onSuccess, onFail);
                <?php } ?>

            });
        });
    </script>

</head>
<body>

<?php require_once $config['serverRoot'] . '/partials/nav.php' ?>

<main role="main" class="container">
    <?php if (isset($request)) {?>
    <h1>Edit Request</h1>
    <?php } else { ?>
    <h1>Request Drives</h1>
    <?php } ?>
    <form id="driveForm" novalidate>
        <div class="row">
            <div class="form-group col-4">
                <label for="forClass">Class</label>
                <input id="forClass" type="text" class="form-control" placeholder="CIS 312" value="<?php if (isset($request)) { echo htmlspecialchars($request->class); }?>" required>
                <div class="invalid-feedback">Class is required</div>
            </div>
            <div class="form-group col-4">
                <label for="numberOfDrives">Number of Drives</label>
                <input type="number" class="form-control" id="numberOfDrives" placeholder="28" value="<?php if (isset($request)) { echo htmlspecialchars($request->drives); }?>" required/>
                <div class="invalid-feedback">Number of Drives is required</div>
            </div>
            <div class="form-group col-4">
                <label for="operatingSystem">Operating System</label>
                <input type="text" class="form-control" id="operatingSystem" placeholder="Windows 10" value="<?php if (isset($request)) { echo htmlspecialchars($request->operatingSystem); }?>" required/>
                <div class="invalid-feedback">Operating System is required</div>
            </div>
        </div>

        <div class="form-group">
            <label for="softwareAndOtherRequirements">Software / Other Requirements</label>
            <textarea class="form-control" id="softwareAndOtherRequirements" rows="10"><?php if (isset($request)) { echo htmlspecialchars($request->other); }?></textarea>
        </div>
        <?php if (isset($request)) {?>
        <button class="btn btn-lg btn-primary float-right" type="submit">Edit Request</button>
        <?php } else { ?>
        <button class="btn btn-lg btn-primary float-right" type="submit">Submit Request</button>
        <?php } ?>

    </form>
</main>

<?php require_once $config['serverRoot'] . '/partials/footer.php' ?>
</body>
</html>